**Simple loan investment DEMO application.**

***Production host***
Demo hosted at: 
http://wandoo-rest.paas.leviracloud.eu/

****REST web service entry points:****
1. receive a loan from loan issuing company
GET ws/loan/import

2. receive a payment from loan issuing company (with allocation of investors income)
GET ws/payment/import

3. available loans for the investment list
GET /ws/loan/investment-ready-list

4. user register 
POST ws/investor/register

5. invest in loan (investor balance and available to invest to be changed)
POST ws/loan/invest

6. add funds to investor
POST ws/back-office/investor/funds/add

7. investor balance
GET /ws/investor/{id}/balance

8. currently authorized investor profile data (TMP this return static user for demo) 
GET ws/investor/get-authorized

9. investor profile data by id
GET ws/investor/{id}

10. investment to lon by id
GET: ws/loan-investment/{id}

11. investments list by investor
GET: ws/loan-investment/by-investor/{id}

12. loan payment by id
GET ws/payment/{id}

14.loan 
GET ws/loan/{id}by id

some JSON data examples see in: \src\main\resources\test-json

****Front-end:****
see index page of prod or localhost
1. list of available loans 
2. possibility to invest (changing investor balance)
3. list of current user investments
4. block lending if full invested

****Tests coverage:****
1. acceptance tests for all general business required work-flows

***Database***
There is 2 options for localhost: use of local DB or remote DB. (see application.properties params)
Also possible reinitialize demo data. (see application.properties params)