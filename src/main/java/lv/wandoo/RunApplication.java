package lv.wandoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages={"lv.wandoo"})
@EnableAutoConfiguration
public class RunApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(RunApplication.class, args);

		context.getBean(DemoDataInitializer.class).initialize();
	}
}
