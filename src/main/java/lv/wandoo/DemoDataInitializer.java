package lv.wandoo;

import com.google.common.collect.ImmutableList;
import lv.wandoo.domain.dto.investment.InvestmentBuilder;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.dto.investor.InvestorRegistrationBuilder;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChangeBuilder;
import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.dto.loan.ImportLoanBuilder;
import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.dto.payment.ImportLoanPaymentBuilder;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.value.BorrowerDetailsBuilder;
import lv.wandoo.domain.model.value.PersonType;
import lv.wandoo.domain.model.value.TermUnit;
import lv.wandoo.domain.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Component
class DemoDataInitializer {
	private final static ImmutableList<InvestorRegistration> INVESTOR_REGISTRATIONS =
			ImmutableList.of(
					new InvestorRegistrationBuilder()
							.email("demo-investor1@gmail.com")
							.password("pass")
							.countryCode("LV")
							.personType(PersonType.PRIVATE)
							.build(),
					new InvestorRegistrationBuilder()
							.email("demo-investor2@gmail.com")
							.password("pass")
							.countryCode("LT")
							.personType(PersonType.LEGAL)
							.build()
			);

	private final static ImmutableList<ImportLoan> IMPORT_LOANS =
			ImmutableList.of(
					new ImportLoanBuilder()
							.importSourceCode("lv_demosource1")
							.importLoanCode("demo_loancode1")
							.issueDate(LocalDate.now().minusMonths(1))
							.amount(new BigDecimal("500"))
							.termUnit(TermUnit.MONTH)
							.term(6)
							.countryCode("LV")
							.interestRate(12.5f)
							.borrowerDetails(
									new BorrowerDetailsBuilder()
											.personType(PersonType.PRIVATE)
											.fullName("Jānis Zariņš")
											.identificationNumber("010101-010101")
											.build()
							)
							.build(),
					new ImportLoanBuilder()
							.importSourceCode("lv_demosource2")
							.importLoanCode("demo_loancode2")
							.issueDate(LocalDate.now().minusMonths(24))
							.amount(new BigDecimal("5000"))
							.termUnit(TermUnit.DAY)
							.term(20)
							.countryCode("UK")
							.interestRate(22.5f)
							.borrowerDetails(
									new BorrowerDetailsBuilder()
											.personType(PersonType.LEGAL)
											.companyName("Export Auto")
											.identificationNumber("020202-010101")
											.build()
							)
							.build(),
					new ImportLoanBuilder()
							.importSourceCode("lv_demosource3")
							.importLoanCode("demo_loancode3")
							.issueDate(LocalDate.now().minusMonths(3))
							.amount(new BigDecimal("5850"))
							.termUnit(TermUnit.YEAR)
							.term(1)
							.countryCode("LT")
							.interestRate(14.5f)
							.borrowerDetails(
									new BorrowerDetailsBuilder()
											.personType(PersonType.LEGAL)
											.companyName("sia SpecialOffer")
											.identificationNumber("REG12123232")
											.build()
							)
							.build(),
					new ImportLoanBuilder()
							.importSourceCode("lv_demosource4")
							.importLoanCode("demo_loancode4")
							.issueDate(LocalDate.now().minusMonths(8))
							.amount(new BigDecimal("5850"))
							.termUnit(TermUnit.YEAR)
							.term(1)
							.countryCode("CZ")
							.interestRate(12.75f)
							.borrowerDetails(
									new BorrowerDetailsBuilder()
											.personType(PersonType.PRIVATE)
											.fullName("Jāna Zariņa")
											.identificationNumber("REG12332211")
											.build()
							)
							.build()
			);

	private final static ImmutableList<ImportLoanPayment> IMPORT_PAYMENTS =
			ImmutableList.of(
					new ImportLoanPaymentBuilder()
							.principal(new BigDecimal("15"))
							.interest(new BigDecimal("15"))
							.penalty(BigDecimal.ZERO)
							.total(new BigDecimal("30"))
							.paymentDate(LocalDate.now().minusDays(1))
							.importSourceCode("lv_demosource1")
							.importLoanCode("demo_loancode1")
							.build(),
					new ImportLoanPaymentBuilder()
							.principal(new BigDecimal("12"))
							.interest(new BigDecimal("4"))
							.penalty(BigDecimal.ZERO)
							.total(new BigDecimal("16"))
							.paymentDate(LocalDate.now().minusDays(2))
							.importSourceCode("lv_demosource1")
							.importLoanCode("demo_loancode1")
							.build(),
					new ImportLoanPaymentBuilder()
							.principal(new BigDecimal("20"))
							.interest(new BigDecimal("2"))
							.penalty(new BigDecimal("18"))
							.total(new BigDecimal("40"))
							.paymentDate(LocalDate.now().minusDays(2))
							.importSourceCode("lv_demosource1")
							.importLoanCode("demo_loancode1")
							.build()
			);

	private InvestorService investorService;
	private LoanService loanService;
	private LoanImportService loanImportService;
	private LoanInvestmentService investmentService;
	private LoanPaymentService loanPaymentService;

	@Value("${wandoo.init-demo-data:false}")
	private boolean initDemoData;

	@Autowired
	public DemoDataInitializer(
			InvestorService investorService,
			LoanImportService loanImportService,
			LoanService loanService,
			LoanInvestmentService investmentService,
			LoanPaymentService loanPaymentService
	) {
		this.investorService = investorService;
		this.loanImportService = loanImportService;
		this.loanService = loanService;
		this.investmentService = investmentService;
		this.loanPaymentService = loanPaymentService;
	}

	void initialize() {
		if (!initDemoData) {
			return;
		}

		List<Investor> investorsCreated = registerInvestors();
		addFundsToInvestors(investorsCreated);

		importLoans();
		investToLoans(investorsCreated);
		importPayments();
	}

	private List<Investor> registerInvestors() {
		INVESTOR_REGISTRATIONS.forEach(
				investorRegistration -> investorService.register(investorRegistration)
		);

		return investorService.getInvestorsFullList();
	}

	private void addFundsToInvestors(List<Investor> investorsCreated) {
		investorsCreated.forEach(
				investor -> investorService.addFunds(new InvestorFundsChangeBuilder()
						.amount(new BigDecimal("10000"))
						.investorId(investor.getId())
						.build())
		);
	}

	private void importLoans() {
		IMPORT_LOANS.forEach(
				importLoan -> loanImportService.importLoan(importLoan)
		);
	}

	private void investToLoans(List<Investor> investorsCreated) {
		List<InvestmentLoan> investmentLoans = loanService.getInvestmentReadyLoans();

		investmentLoans.forEach(investmentLoan -> investmentService.investToLoan(
				new InvestmentBuilder()
						.loanId(investmentLoan.getId())
						.investorId(investorsCreated.get(0).getId())
						.amount(new BigDecimal("100"))
						.build())
		);
	}

	private void importPayments() {
		IMPORT_PAYMENTS.forEach(
				loanPayment -> loanPaymentService.importLoanPayment(loanPayment)
		);
	}
}
