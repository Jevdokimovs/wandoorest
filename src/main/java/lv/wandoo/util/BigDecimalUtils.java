package lv.wandoo.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtils {
	public final static RoundingMode MONEY_ROUNDING = RoundingMode.HALF_EVEN;
	public final static int MONEY_SCALE = 2;

	public static boolean isEqual(
			BigDecimal amountCompareOf,
			BigDecimal amountCompareTo
	) {
		return moneyScaleFormat(amountCompareOf)
				.equals(moneyScaleFormat(amountCompareTo));
	}

	public static boolean isGreatOrEquals(
			BigDecimal amountCompareOf,
			BigDecimal amountCompareTo
	) {
		return amountCompareOf.compareTo(amountCompareTo) > 0 ||
				amountCompareOf.compareTo(amountCompareTo) == 0;
	}

	public static boolean isPositive(BigDecimal amount) {
		return amount.signum() > 0;
	}

	public static BigDecimal moneyScaleFormat(BigDecimal amount) {
		return amount.setScale(MONEY_SCALE, MONEY_ROUNDING);
	}
}
