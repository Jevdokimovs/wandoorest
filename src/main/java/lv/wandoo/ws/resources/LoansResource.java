package lv.wandoo.ws.resources;

import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.service.LoanImportService;
import lv.wandoo.domain.service.LoanService;
import lv.wandoo.domain.validation.ImportLoanValidator;
import lv.wandoo.domain.validation.ValidationResult;
import lv.wandoo.ws.config.JerseyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/loan")
@Component
public class LoansResource {
	private LoanService loanService;
	private LoanImportService loanImportService;
	private ImportLoanValidator importLoanValidator;

	@Autowired
	public LoansResource(
			LoanService loanService,
			LoanImportService loanImportService,
			ImportLoanValidator importLoanValidator
	) {
		this.loanService = loanService;
		this.loanImportService = loanImportService;
		this.importLoanValidator = importLoanValidator;
	}

	@Path("investment-ready-list")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getInvestmentReadyLoans() {
		List<InvestmentLoan> list = loanService.getInvestmentReadyLoans();

		return Response.ok(list).build();
	}

	@Path("import")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response importLoan(ImportLoan importLoan) {
		ValidationResult validationResult = importLoanValidator.validate(importLoan);
		if (!validationResult.hasErrors()) {
			loanImportService.importLoan(importLoan);
		}

		return Response.ok(validationResult).build();
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getLoan(@PathParam("id") Long loanId) {
		InvestmentLoan investmentLoan = loanService.getInvestmentLoan(loanId);

		return Response.ok(investmentLoan).build();
	}
}
