package lv.wandoo.ws.resources.backoffice;

import lv.wandoo.domain.dto.investor.balance.InvestorFundsChange;
import lv.wandoo.domain.service.InvestorService;
import lv.wandoo.domain.validation.InvestorFundsChangeValidator;
import lv.wandoo.domain.validation.ValidationResult;
import lv.wandoo.ws.config.JerseyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/back-office/investor/funds")
@Component
public class InvestorFundsResource {
	private InvestorService investorService;
	private InvestorFundsChangeValidator investorFundsChangeValidator;

	@Autowired
	public InvestorFundsResource(
			InvestorService investorService,
			InvestorFundsChangeValidator investorFundsChangeValidator
	) {
		this.investorService = investorService;
		this.investorFundsChangeValidator = investorFundsChangeValidator;
	}

	@Path("add")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response registerInvestor(InvestorFundsChange investorFundsChange) {
		ValidationResult validationResult = investorFundsChangeValidator.validate(investorFundsChange);
		if(!validationResult.hasErrors()) {
			investorService.addFunds(investorFundsChange);
		}

		return Response.ok(validationResult).build();
	}
}
