package lv.wandoo.ws.resources;

import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.dto.payment.LoanPaymentListItem;
import lv.wandoo.domain.service.LoanPaymentService;
import lv.wandoo.domain.validation.ImportLoanPaymentValidator;
import lv.wandoo.domain.validation.ValidationResult;
import lv.wandoo.ws.config.JerseyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/payment")
@Component
public class LoanPaymentsResource {
	private LoanPaymentService loanPaymentService;
	private ImportLoanPaymentValidator importLoanPaymentValidator;

	@Autowired
	public LoanPaymentsResource(
			LoanPaymentService loanPaymentService,
			ImportLoanPaymentValidator importLoanPaymentValidator
	) {
		this.loanPaymentService = loanPaymentService;
		this.importLoanPaymentValidator = importLoanPaymentValidator;
	}

	@Path("import")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response importLoanPayment(ImportLoanPayment importLoanPayment) {
		ValidationResult validationResult
				= importLoanPaymentValidator.validate(importLoanPayment);
		if (!validationResult.hasErrors()) {
			loanPaymentService.importLoanPayment(importLoanPayment);
		}

		return Response.ok(validationResult).build();
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getLoan(@PathParam("id") Long id) {
		LoanPaymentListItem loanPaymentListItem 
				= loanPaymentService.getLoanPaymentListItem(id);

		return Response.ok(loanPaymentListItem).build();
	}
}
