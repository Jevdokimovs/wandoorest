package lv.wandoo.ws.resources;

import lv.wandoo.domain.dto.investment.Investment;
import lv.wandoo.domain.dto.investment.InvestorMoneyAsset;
import lv.wandoo.domain.service.LoanInvestmentService;
import lv.wandoo.domain.validation.InvestmentValidator;
import lv.wandoo.domain.validation.ValidationResult;
import lv.wandoo.ws.config.JerseyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/loan-investment")
@Component
public class LoanInvestmentsResource {
	private LoanInvestmentService loanInvestmentService;
	private InvestmentValidator investmentValidator;

	@Autowired
	public LoanInvestmentsResource(
			LoanInvestmentService loanInvestmentService,
			InvestmentValidator investmentValidator
	) {
		this.loanInvestmentService = loanInvestmentService;
		this.investmentValidator = investmentValidator;
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getInvestorMoneyAssetById(@PathParam("id") Long loanInvestmentId) {
		InvestorMoneyAsset investorMoneyAsset = loanInvestmentService
				.getInvestorMoneyAsset(loanInvestmentId);

		return Response.ok(investorMoneyAsset).build();
	}

	@Path("invest")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response invest(Investment investment) {
		ValidationResult validationResult = investmentValidator.validate(investment);
		if (!validationResult.hasErrors()) {
			loanInvestmentService.investToLoan(investment);
		}

		return Response.ok(validationResult).build();
	}

	@Path("by-investor/{investorId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getInvestorMoneyAssetByInvestorId(@PathParam("investorId") Long investorId) {
		List<InvestorMoneyAsset> list 
				= loanInvestmentService.getInvestorMoneyAssetsByInvestor(investorId);

		return Response.ok(list).build();
	}
}
