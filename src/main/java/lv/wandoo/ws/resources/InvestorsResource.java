package lv.wandoo.ws.resources;

import lv.wandoo.domain.dto.investor.InvestorProfile;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.service.InvestorService;
import lv.wandoo.domain.validation.InvestorRegistrationValidator;
import lv.wandoo.domain.validation.ValidationResult;
import lv.wandoo.ws.config.JerseyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/investor")
@Component
public class InvestorsResource {
	private InvestorService investorService;
	private InvestorRegistrationValidator investorRegistrationValidator;

	@Autowired
	public InvestorsResource(
			InvestorService investorService,
			InvestorRegistrationValidator investorRegistrationValidator
	) {
		this.investorService = investorService;
		this.investorRegistrationValidator = investorRegistrationValidator;
	}

	@Path("register")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response registerInvestor(InvestorRegistration investorRegistration) {
		ValidationResult validationResult = investorRegistrationValidator.validate(investorRegistration);
		if (!validationResult.hasErrors()) {
			investorService.register(investorRegistration);
		}

		return Response.ok(validationResult).build();
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getInvestor(@PathParam("id") Long investorId) {
		InvestorProfile investorProfile = investorService.getInvestorProfile(investorId);

		return Response.ok(investorProfile).build();
	}

	@Path("{id}/balance")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getInvestorBalance(@PathParam("id") Long investorId) {
		InvestorBalance investorBalance = investorService.getBalance(investorId);

		return Response.ok(investorBalance).build();
	}

	@Path("/get-authorized")
	@GET
	@Produces(MediaType.APPLICATION_JSON + JerseyConstants.CHARSET_UTF8)
	public Response getCurrentAuthorizedInvestor() {
		InvestorProfile investorProfile = investorService.getAuthorizedInvestor();

		return Response.ok(investorProfile).build();
	}
}
