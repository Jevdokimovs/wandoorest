package lv.wandoo.ws.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.wandoo.ws.resources.InvestorsResource;
import lv.wandoo.ws.resources.LoanInvestmentsResource;
import lv.wandoo.ws.resources.LoanPaymentsResource;
import lv.wandoo.ws.resources.LoansResource;
import lv.wandoo.ws.resources.backoffice.InvestorFundsResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("ws")
public class JerseyAppResourceConfig extends ResourceConfig {
	@Autowired
	public JerseyAppResourceConfig(ObjectMapper objectMapper) {
		//unfortunately jersey have issue working from jar on production
		//fixed by registering every resource manually:(

		//packages("lv.wandoo.ws.resources");
		register(new JacksonContextResolveProvider(objectMapper));
		register(InvestorFundsResource.class);
		register(InvestorsResource.class);
		register(LoanInvestmentsResource.class);
		register(LoanPaymentsResource.class);
		register(LoansResource.class);
	}
}
