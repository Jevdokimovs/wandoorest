package lv.wandoo.ws.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import static com.fasterxml.jackson.databind.DeserializationFeature.READ_ENUMS_USING_TO_STRING;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_ENUMS_USING_TO_STRING;

@Provider
public class JacksonContextResolveProvider implements ContextResolver<ObjectMapper> {
	private final ObjectMapper mapper;

	JacksonContextResolveProvider(ObjectMapper mapper) {
		this.mapper = mapper;

		mapper.configure(
				SerializationFeature.INDENT_OUTPUT,
				true);
		mapper.configure(
				DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
				false
		);

		mapper.enable(WRITE_ENUMS_USING_TO_STRING);
		mapper.enable(READ_ENUMS_USING_TO_STRING);
	}

	@Override
	public ObjectMapper getContext(Class<?> type) {
		return mapper;
	}
}
