package lv.wandoo.domain.dto;

import lv.wandoo.domain.model.common.BaseEntity;

import java.util.List;
import java.util.stream.Collectors;

public interface DTOConverter<D extends DTO, E extends BaseEntity> {
	D toDTO(E entity);

	default List<D> toDTOList(List<E> list) {
		return list.stream().map(this::toDTO).collect(Collectors.toList());
	}
}
