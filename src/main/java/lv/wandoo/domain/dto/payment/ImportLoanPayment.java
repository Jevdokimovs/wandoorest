package lv.wandoo.domain.dto.payment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ImportLoanPayment implements DTO {
	private String importSourceCode;

	private String importLoanCode;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate paymentDate;

	private BigDecimal principal;

	private BigDecimal interest;

	private BigDecimal penalty;

	private BigDecimal total;

	@SuppressWarnings("unused")
	ImportLoanPayment() {
	}

	ImportLoanPayment(
			String importSourceCode,
			String importLoanCode,
			LocalDate paymentDate,
			BigDecimal principal,
			BigDecimal interest,
			BigDecimal penalty,
			BigDecimal total
	) {
		this.importSourceCode = importSourceCode;
		this.importLoanCode = importLoanCode;
		this.paymentDate = paymentDate;
		this.principal = principal;
		this.interest = interest;
		this.penalty = penalty;
		this.total = total;
	}

	public String getImportSourceCode() {
		return importSourceCode;
	}

	public void setImportSourceCode(String importSourceCode) {
		this.importSourceCode = importSourceCode;
	}

	public String getImportLoanCode() {
		return importLoanCode;
	}

	public void setImportLoanCode(String importLoanCode) {
		this.importLoanCode = importLoanCode;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getPenalty() {
		return penalty;
	}

	public void setPenalty(BigDecimal penalty) {
		this.penalty = penalty;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("importSourceCode", importSourceCode)
				.add("importLoanCode", importLoanCode)
				.add("paymentDate", paymentDate)
				.add("principal", principal)
				.add("interest", interest)
				.add("penalty", penalty)
				.add("total", total)
				.toString();
	}
}
