package lv.wandoo.domain.dto.payment;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ImportLoanPaymentBuilder {
	private String importSourceCode;
	private String importLoanCode;
	private LocalDate paymentDate;
	private BigDecimal principal;
	private BigDecimal interest;
	private BigDecimal penalty;
	private BigDecimal total;

	public ImportLoanPaymentBuilder importSourceCode(String importSourceCode) {
		this.importSourceCode = importSourceCode;
		return this;
	}

	public ImportLoanPaymentBuilder importLoanCode(String importLoanCode) {
		this.importLoanCode = importLoanCode;
		return this;
	}

	public ImportLoanPaymentBuilder paymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
		return this;
	}

	public ImportLoanPaymentBuilder principal(BigDecimal principal) {
		this.principal = principal;
		return this;
	}

	public ImportLoanPaymentBuilder interest(BigDecimal interest) {
		this.interest = interest;
		return this;
	}

	public ImportLoanPaymentBuilder penalty(BigDecimal penalty) {
		this.penalty = penalty;
		return this;
	}

	public ImportLoanPaymentBuilder total(BigDecimal total) {
		this.total = total;
		return this;
	}

	public ImportLoanPayment build() {
		return new ImportLoanPayment(importSourceCode, importLoanCode, paymentDate, principal, interest, penalty, total);
	}
}