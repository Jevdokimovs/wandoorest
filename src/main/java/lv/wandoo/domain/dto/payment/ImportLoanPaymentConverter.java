package lv.wandoo.domain.dto.payment;

import lv.wandoo.domain.dto.EntityConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanPayment;
import lv.wandoo.domain.service.LoanImportService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImportLoanPaymentConverter implements EntityConverter<LoanPayment, ImportLoanPayment> {
	private LoanImportService loanImportService;

	@Autowired
	public ImportLoanPaymentConverter(LoanImportService loanImportService) {
		this.loanImportService = loanImportService;
	}

	@Override
	public LoanPayment toEntity(ImportLoanPayment importLoanPayment) {
		LoanPayment entity = new LoanPayment();
		BeanUtils.copyProperties(
				importLoanPayment,
				entity,
				"loanId"
		);

		Loan loan = loanImportService.getImportedLoan(
				importLoanPayment.getImportSourceCode(),
				importLoanPayment.getImportLoanCode()
		).orElseThrow(IllegalStateException::new);
		entity.setLoan(loan);

		return entity;
	}
}
