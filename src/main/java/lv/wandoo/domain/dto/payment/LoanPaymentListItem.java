package lv.wandoo.domain.dto.payment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LoanPaymentListItem implements DTO {
	private Long loanId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate paymentDate;

	private BigDecimal principal;

	private BigDecimal interest;

	private BigDecimal penalty;

	private BigDecimal total;

	@SuppressWarnings("unused")
	public LoanPaymentListItem() {
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getPenalty() {
		return penalty;
	}

	public void setPenalty(BigDecimal penalty) {
		this.penalty = penalty;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("loanId", loanId)
				.add("paymentDate", paymentDate)
				.add("principal", principal)
				.add("interest", interest)
				.add("penalty", penalty)
				.add("total", total)
				.toString();
	}
}
