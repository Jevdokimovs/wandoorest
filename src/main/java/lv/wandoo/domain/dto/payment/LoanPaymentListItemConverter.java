package lv.wandoo.domain.dto.payment;

import lv.wandoo.domain.dto.DTOConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanPayment;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class LoanPaymentListItemConverter implements DTOConverter<LoanPaymentListItem, LoanPayment> {
	@Override
	public LoanPaymentListItem toDTO(LoanPayment loanPayment) {
		LoanPaymentListItem loanPaymentListItem = new LoanPaymentListItem();
		BeanUtils.copyProperties(
				loanPayment,
				loanPaymentListItem,
				"loan"
		);

		Loan loan = loanPayment.getLoan();
		loanPaymentListItem.setLoanId(loan.getId());
		return loanPaymentListItem;
	}
}
