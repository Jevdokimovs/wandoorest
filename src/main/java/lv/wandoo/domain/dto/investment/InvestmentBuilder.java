package lv.wandoo.domain.dto.investment;

import java.math.BigDecimal;

public class InvestmentBuilder {
	private Long loanId;
	private Long investorId;
	private BigDecimal amount;

	public InvestmentBuilder loanId(Long loanId) {
		this.loanId = loanId;
		return this;
	}

	public InvestmentBuilder investorId(Long investorId) {
		this.investorId = investorId;
		return this;
	}

	public InvestmentBuilder amount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}

	public Investment build() {
		return new Investment(loanId, investorId, amount);
	}
}