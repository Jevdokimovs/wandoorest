package lv.wandoo.domain.dto.investment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;
import lv.wandoo.domain.model.value.TermUnit;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InvestorMoneyAsset implements DTO {
	private Long id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate investmentDate;
	private Long loanId;
	private Long investorId;
	private BigDecimal amount;
	private Integer term;
	private TermUnit termUnit;
	private Float interestRate;

	@SuppressWarnings("unused")
	public InvestorMoneyAsset() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getInvestmentDate() {
		return investmentDate;
	}

	public void setInvestmentDate(LocalDate investmentDate) {
		this.investmentDate = investmentDate;
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public TermUnit getTermUnit() {
		return termUnit;
	}

	public void setTermUnit(TermUnit termUnit) {
		this.termUnit = termUnit;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("investmentDate", investmentDate)
				.add("loanId", loanId)
				.add("investorId", investorId)
				.add("amount", amount)
				.add("term", term)
				.add("termUnit", termUnit)
				.add("interestRate", interestRate)
				.toString();
	}
}