package lv.wandoo.domain.dto.investment;

import lv.wandoo.domain.dto.EntityConverter;
import lv.wandoo.domain.model.LoanInvestment;
import lv.wandoo.domain.repository.InvestorRepository;
import lv.wandoo.domain.repository.LoanRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvestmentConverter implements EntityConverter<LoanInvestment, Investment> {
	private InvestorRepository investorRepository;
	private LoanRepository loanRepository;

	@Autowired
	public InvestmentConverter(InvestorRepository investorRepository, LoanRepository loanRepository) {
		this.investorRepository = investorRepository;
		this.loanRepository = loanRepository;
	}

	@Override
	public LoanInvestment toEntity(Investment investment) {
		LoanInvestment entity = new LoanInvestment();
		BeanUtils.copyProperties(
				investment,
				entity,
				"loanId", "investorId"
		);

		entity.setInvestor(investorRepository.findOne(investment.getInvestorId()));
		entity.setLoan(loanRepository.findOne(investment.getLoanId()));

		return entity;
	}
}
