package lv.wandoo.domain.dto.investment;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;

import java.math.BigDecimal;

public class Investment implements DTO {
	private Long loanId;
	private Long investorId;
	private BigDecimal amount;

	@SuppressWarnings("unused")
	Investment() {
	}

	Investment(Long loanId, Long investorId, BigDecimal amount) {
		this.loanId = loanId;
		this.investorId = investorId;
		this.amount = amount;
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("loanId", loanId)
				.add("investorId", investorId)
				.add("amount", amount)
				.toString();
	}
}
