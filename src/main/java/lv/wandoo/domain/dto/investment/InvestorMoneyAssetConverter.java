package lv.wandoo.domain.dto.investment;

import lv.wandoo.domain.dto.DTOConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class InvestorMoneyAssetConverter implements DTOConverter<InvestorMoneyAsset, LoanInvestment> {
	@Override
	public InvestorMoneyAsset toDTO(LoanInvestment loanInvestment) {
		InvestorMoneyAsset investorMoneyAsset = new InvestorMoneyAsset();
		BeanUtils.copyProperties(
				loanInvestment,
				investorMoneyAsset,
				"loan", "investor"
		);

		investorMoneyAsset.setInvestmentDate(loanInvestment.getCreated().toLocalDate());
		Loan loan = loanInvestment.getLoan();
		investorMoneyAsset.setLoanId(loan.getId());
		investorMoneyAsset.setTerm(loan.getTerm());
		investorMoneyAsset.setTermUnit(loan.getTermUnit());
		investorMoneyAsset.setInterestRate(loan.getInterestRate());
		investorMoneyAsset.setInvestorId(loanInvestment.getInvestor().getId());

		return investorMoneyAsset;
	}
}
