package lv.wandoo.domain.dto;

import lv.wandoo.domain.model.common.BaseEntity;

public interface EntityConverter<E extends BaseEntity, D extends DTO> {
	E toEntity(D dto);
}
