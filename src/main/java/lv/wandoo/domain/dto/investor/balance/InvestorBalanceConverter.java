package lv.wandoo.domain.dto.investor.balance;

import lv.wandoo.domain.dto.DTOConverter;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.value.ClientBalance;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class InvestorBalanceConverter implements DTOConverter<InvestorBalance, Investor> {
	@Override
	public InvestorBalance toDTO(Investor investor) {
		InvestorBalance investorBalance = new InvestorBalance();

		investorBalance.setAvailable(investor.getClientBalance().map(
				ClientBalance::getAvailable
		).orElse(BigDecimal.ZERO));

		return investorBalance;
	}
}
