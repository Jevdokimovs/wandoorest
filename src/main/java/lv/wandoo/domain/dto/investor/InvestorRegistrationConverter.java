package lv.wandoo.domain.dto.investor;


import lv.wandoo.domain.dto.EntityConverter;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.util.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class InvestorRegistrationConverter implements EntityConverter<Investor, InvestorRegistration> {
	@Override
	public Investor toEntity(InvestorRegistration investorRegistration) {
		Investor entity = new Investor();
		BeanUtils.copyProperties(investorRegistration, entity);

		entity.setPassword(SecurityUtils.encodePassword(investorRegistration.getPassword()));
		entity.setHash(SecurityUtils.generateHashFromEmail(investorRegistration.getEmail()));

		return entity;
	}
}
