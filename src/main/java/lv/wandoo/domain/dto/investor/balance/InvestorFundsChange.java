package lv.wandoo.domain.dto.investor.balance;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;

import java.math.BigDecimal;

public class InvestorFundsChange implements DTO {
	private Long investorId;
	private BigDecimal amount;

	@SuppressWarnings("unused")
	InvestorFundsChange() {
	}

	InvestorFundsChange(Long investorId, BigDecimal amount) {
		this.investorId = investorId;
		this.amount = amount;
	}

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("investorId", investorId)
				.add("amount", amount)
				.toString();
	}
}
