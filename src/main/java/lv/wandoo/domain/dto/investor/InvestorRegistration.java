package lv.wandoo.domain.dto.investor;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;
import lv.wandoo.domain.model.value.PersonType;

public class InvestorRegistration implements DTO {
	private String email;
	private String password;
	private PersonType personType;
	private String countryCode;

	@SuppressWarnings("unused")
	InvestorRegistration() {
	}

	InvestorRegistration(
			String email,
			String password,
			PersonType personType,
			String countryCode
	) {
		this.email = email;
		this.password = password;
		this.personType = personType;
		this.countryCode = countryCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(PersonType personType) {
		this.personType = personType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("email", email)
				.add("password", password)
				.add("personType", personType)
				.add("countryCode", countryCode)
				.toString();
	}
}
