package lv.wandoo.domain.dto.investor;

import lv.wandoo.domain.model.value.PersonType;

public class InvestorRegistrationBuilder {
	private String email;
	private String password;
	private PersonType personType;
	private String countryCode;

	public InvestorRegistrationBuilder email(String email) {
		this.email = email;
		return this;
	}

	public InvestorRegistrationBuilder password(String password) {
		this.password = password;
		return this;
	}

	public InvestorRegistrationBuilder personType(PersonType personType) {
		this.personType = personType;
		return this;
	}

	public InvestorRegistrationBuilder countryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public InvestorRegistration build() {
		return new InvestorRegistration(email, password, personType, countryCode);
	}
}