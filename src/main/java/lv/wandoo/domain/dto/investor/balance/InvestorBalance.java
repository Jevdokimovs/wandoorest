package lv.wandoo.domain.dto.investor.balance;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;

import java.math.BigDecimal;

public class InvestorBalance implements DTO {
	private BigDecimal available;

	@SuppressWarnings("unused")
	InvestorBalance() {
	}

	public BigDecimal getAvailable() {
		return available;
	}

	public void setAvailable(BigDecimal available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("available", available)
				.toString();
	}
}
