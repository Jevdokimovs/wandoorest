package lv.wandoo.domain.dto.investor.balance;

import java.math.BigDecimal;

public class InvestorFundsChangeBuilder {
	private Long investorId;
	private BigDecimal amount;

	public InvestorFundsChangeBuilder investorId(Long investorId) {
		this.investorId = investorId;
		return this;
	}

	public InvestorFundsChangeBuilder amount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}

	public InvestorFundsChange build() {
		return new InvestorFundsChange(investorId, amount);
	}
}