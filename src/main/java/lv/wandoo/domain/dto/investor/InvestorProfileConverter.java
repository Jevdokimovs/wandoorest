package lv.wandoo.domain.dto.investor;

import lv.wandoo.domain.dto.DTOConverter;
import lv.wandoo.domain.model.Investor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class InvestorProfileConverter implements DTOConverter<InvestorProfile, Investor> {
	@Override
	public InvestorProfile toDTO(Investor investor) {
		InvestorProfile investorProfile = new InvestorProfile();
		BeanUtils.copyProperties(
				investor,
				investorProfile
		);

		return investorProfile;
	}
}
