package lv.wandoo.domain.dto.loan;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;
import lv.wandoo.domain.model.value.BorrowerDetails;
import lv.wandoo.domain.model.value.TermUnit;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ImportLoan implements DTO {
	private String importSourceCode;

	private String importLoanCode;

	private BorrowerDetails borrowerDetails;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate issueDate;

	private BigDecimal amount;

	private int term;

	private TermUnit termUnit;

	private Float interestRate;

	private String countryCode;

	@SuppressWarnings("unused")
	ImportLoan() {
	}

	ImportLoan(
			String importSourceCode, 
			String importLoanCode, 
			BorrowerDetails borrowerDetails, 
			LocalDate issueDate, 
			BigDecimal amount, 
			int term, 
			TermUnit termUnit, 
			Float interestRate, 
			String countryCode
	) {
		this.importSourceCode = importSourceCode;
		this.importLoanCode = importLoanCode;
		this.borrowerDetails = borrowerDetails;
		this.issueDate = issueDate;
		this.amount = amount;
		this.term = term;
		this.termUnit = termUnit;
		this.interestRate = interestRate;
		this.countryCode = countryCode;
	}

	public String getImportSourceCode() {
		return importSourceCode;
	}

	public void setImportSourceCode(String importSourceCode) {
		this.importSourceCode = importSourceCode;
	}

	public String getImportLoanCode() {
		return importLoanCode;
	}

	public void setImportLoanCode(String importLoanCode) {
		this.importLoanCode = importLoanCode;
	}

	public BorrowerDetails getBorrowerDetails() {
		return borrowerDetails;
	}

	public void setBorrowerDetails(BorrowerDetails borrowerDetails) {
		this.borrowerDetails = borrowerDetails;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public TermUnit getTermUnit() {
		return termUnit;
	}

	public void setTermUnit(TermUnit termUnit) {
		this.termUnit = termUnit;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("importSourceCode", importSourceCode)
				.add("importLoanCode", importLoanCode)
				.add("borrowerDetails", borrowerDetails)
				.add("issueDate", issueDate)
				.add("amount", amount)
				.add("term", term)
				.add("termUnit", termUnit)
				.add("interestRate", interestRate)
				.add("countryCode", countryCode)
				.toString();
	}
}
