package lv.wandoo.domain.dto.loan;

import lv.wandoo.domain.dto.EntityConverter;
import lv.wandoo.domain.model.Loan;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class ImportLoanConverter implements EntityConverter<Loan, ImportLoan> {
	@Override
	public Loan toEntity(ImportLoan importLoan) {
		Loan entity = new Loan();
		BeanUtils.copyProperties(
				importLoan,
				entity,
				"published"
		);

		return entity;
	}
}
