package lv.wandoo.domain.dto.loan;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import lv.wandoo.domain.dto.DTO;
import lv.wandoo.domain.model.value.TermUnit;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InvestmentLoan implements DTO {
	private Long id;

	private String borrowerTitle;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate importDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private LocalDate issueDate;

	private BigDecimal amount;

	private int term;

	private TermUnit termUnit;

	private Float interestRate;

	private String countryCode;

	private BigDecimal investedTotal;

	private BigDecimal availableToInvest;

	private boolean investmentAllowed;

	@SuppressWarnings("unused")
	InvestmentLoan() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBorrowerTitle() {
		return borrowerTitle;
	}

	public void setBorrowerTitle(String borrowerTitle) {
		this.borrowerTitle = borrowerTitle;
	}

	public LocalDate getImportDate() {
		return importDate;
	}

	public void setImportDate(LocalDate importDate) {
		this.importDate = importDate;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public TermUnit getTermUnit() {
		return termUnit;
	}

	public void setTermUnit(TermUnit termUnit) {
		this.termUnit = termUnit;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public BigDecimal getInvestedTotal() {
		return investedTotal;
	}

	public void setInvestedTotal(BigDecimal investedTotal) {
		this.investedTotal = investedTotal;
	}

	public BigDecimal getAvailableToInvest() {
		return availableToInvest;
	}

	public void setAvailableToInvest(BigDecimal availableToInvest) {
		this.availableToInvest = availableToInvest;
	}

	public boolean isInvestmentAllowed() {
		return investmentAllowed;
	}

	public void setInvestmentAllowed(boolean investmentAllowed) {
		this.investmentAllowed = investmentAllowed;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("borrowerTitle", borrowerTitle)
				.add("importDate", importDate)
				.add("issueDate", issueDate)
				.add("amount", amount)
				.add("term", term)
				.add("termUnit", termUnit)
				.add("interestRate", interestRate)
				.add("countryCode", countryCode)
				.add("investedTotal", investedTotal)
				.add("availableToInvest", availableToInvest)
				.add("investmentAllowed", investmentAllowed)
				.toString();
	}
}
