package lv.wandoo.domain.dto.loan;

import lv.wandoo.domain.model.value.BorrowerDetails;
import lv.wandoo.domain.model.value.TermUnit;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ImportLoanBuilder {
	private String importSourceCode;
	private String importLoanCode;
	private BorrowerDetails borrowerDetails;
	private LocalDate issueDate;
	private BigDecimal amount;
	private int term;
	private TermUnit termUnit;
	private Float interestRate;
	private String countryCode;

	public ImportLoanBuilder importSourceCode(String importSourceCode) {
		this.importSourceCode = importSourceCode;
		return this;
	}

	public ImportLoanBuilder importLoanCode(String importLoanCode) {
		this.importLoanCode = importLoanCode;
		return this;
	}

	public ImportLoanBuilder borrowerDetails(BorrowerDetails borrowerDetails) {
		this.borrowerDetails = borrowerDetails;
		return this;
	}

	public ImportLoanBuilder issueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
		return this;
	}

	public ImportLoanBuilder amount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}

	public ImportLoanBuilder term(int term) {
		this.term = term;
		return this;
	}

	public ImportLoanBuilder termUnit(TermUnit termUnit) {
		this.termUnit = termUnit;
		return this;
	}

	public ImportLoanBuilder interestRate(Float interestRate) {
		this.interestRate = interestRate;
		return this;
	}

	public ImportLoanBuilder countryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public ImportLoan build() {
		return new ImportLoan(importSourceCode, importLoanCode, borrowerDetails, issueDate, amount, term, termUnit, interestRate, countryCode);
	}
}