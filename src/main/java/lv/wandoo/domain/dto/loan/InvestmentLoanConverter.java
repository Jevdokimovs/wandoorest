package lv.wandoo.domain.dto.loan;

import lv.wandoo.domain.dto.DTOConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.value.BorrowerDetails;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class InvestmentLoanConverter implements DTOConverter<InvestmentLoan, Loan> {
	@Override
	public InvestmentLoan toDTO(Loan entity) {
		InvestmentLoan investmentLoan = new InvestmentLoan();
		BeanUtils.copyProperties(
				entity,
				investmentLoan,
				"created", "updated", "borrowerDetails"
		);
		investmentLoan.setImportDate(entity.getCreated().toLocalDate());

		if (entity.getBorrowerDetails().isPresent()) {
			BorrowerDetails borrowerDetails = entity.getBorrowerDetails().get();

			switch (borrowerDetails.getPersonType()) {
				case LEGAL: {
					investmentLoan.setBorrowerTitle(borrowerDetails.getCompanyName());
					break;
				}
				case PRIVATE: {
					investmentLoan.setBorrowerTitle(borrowerDetails.getFullName());
					break;
				}
				default: {
					investmentLoan.setBorrowerTitle("");
					break;
				}
			}
		}

		investmentLoan.setInvestedTotal(entity.investedTotal());
		investmentLoan.setAvailableToInvest(entity.availableToInvest());
		investmentLoan.setInvestmentAllowed(entity.investmentAllowed());

		return investmentLoan;
	}
}
