package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.common.BaseEntity;

interface BaseRepository<T extends BaseEntity> {
	void persist(T entity);

	void delete(T entity);

	T findOne(Long id);

	Iterable<T> findAll();

	boolean exists(Long id);
}

