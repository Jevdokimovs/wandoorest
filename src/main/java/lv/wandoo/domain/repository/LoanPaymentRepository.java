package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.LoanPayment;
import org.springframework.stereotype.Repository;

@Repository
public class LoanPaymentRepository extends BaseRepositoryImpl<LoanPayment>{
}
