package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.common.BaseEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
abstract class BaseRepositoryImpl<T extends BaseEntity> implements BaseRepository<T> {
	@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
	@Autowired
	private SessionFactory sessionFactory;

	private Class<T> entityClass;

	@SuppressWarnings("unchecked")
	BaseRepositoryImpl() {
		entityClass = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), BaseRepositoryImpl.class);
	}

	@Override
	public void persist(T entity) {
		session().saveOrUpdate(entity);
	}

	@Override
	public void delete(T entity) {
		session().delete(entity);
	}

	@Override
	public T findOne(Long id) {
		if (id == null) {
			return null;
		}

		return session().get(entityClass, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return session().createCriteria(entityClass).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean exists(Long id) {
		return findOne(id) != null;
	}

	Criteria criteria() {
		return session().createCriteria(entityClass);
	}

	private Session session() {
		return sessionFactory.getCurrentSession();
	}
}
