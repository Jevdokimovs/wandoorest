package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.Investor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static org.hibernate.criterion.Restrictions.eq;

@Repository
public class InvestorRepository extends BaseRepositoryImpl<Investor> {
	public Optional<Investor> findByEmail(String email) {
		return Optional.ofNullable((Investor) super.criteria()
				.add(eq("email", email))
				.uniqueResult());
	}
}