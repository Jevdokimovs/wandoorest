package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.Loan;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static org.hibernate.criterion.Restrictions.and;
import static org.hibernate.criterion.Restrictions.eq;

@Repository
public class LoanRepository extends BaseRepositoryImpl<Loan> {
	public Optional<Loan> getImportedLoan(
			String importSourceCode,
			String importLoanCode
	) {
		return Optional.ofNullable((Loan) super.criteria()
				.add(
						and(
								eq("importSourceCode", importSourceCode),
								eq("importLoanCode", importLoanCode)
						)
				).uniqueResult());
	}
}