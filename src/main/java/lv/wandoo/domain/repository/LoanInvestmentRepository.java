package lv.wandoo.domain.repository;

import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.hibernate.criterion.Restrictions.and;
import static org.hibernate.criterion.Restrictions.eq;

@Repository
public class LoanInvestmentRepository extends BaseRepositoryImpl<LoanInvestment> {
	@SuppressWarnings("unchecked")
	public List<LoanInvestment> getByInvestorAndLoan(Loan loan, Investor investor) {
		return super.criteria()
				.add(
						and(
								eq("loan", loan),
								eq("investor", investor)
						)
				).list();
	}
}
