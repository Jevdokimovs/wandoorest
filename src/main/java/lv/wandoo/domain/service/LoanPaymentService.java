package lv.wandoo.domain.service;

import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.dto.payment.ImportLoanPaymentConverter;
import lv.wandoo.domain.dto.payment.LoanPaymentListItem;
import lv.wandoo.domain.dto.payment.LoanPaymentListItemConverter;
import lv.wandoo.domain.model.LoanPayment;
import lv.wandoo.domain.repository.LoanPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoanPaymentService {
	private InvestorIncomeService investorIncomeService;
	private LoanPaymentRepository loanPaymentRepository;
	private ImportLoanPaymentConverter importLoanPaymentConverter;
	private LoanPaymentListItemConverter loanPaymentListItemConverter;

	@Autowired
	public LoanPaymentService(
			InvestorIncomeService investorIncomeService,
			LoanPaymentRepository loanPaymentRepository,
			ImportLoanPaymentConverter importLoanPaymentConverter,
			LoanPaymentListItemConverter loanPaymentListItemConverter
	) {
		this.investorIncomeService = investorIncomeService;
		this.loanPaymentRepository = loanPaymentRepository;
		this.importLoanPaymentConverter = importLoanPaymentConverter;
		this.loanPaymentListItemConverter = loanPaymentListItemConverter;
	}

	@Transactional
	public void importLoanPayment(ImportLoanPayment importLoanPayment) {
		LoanPayment loanPayment = importLoanPaymentConverter
				.toEntity(importLoanPayment);

		loanPaymentRepository.persist(loanPayment);
		investorIncomeService.allocateInvestorsMoney(loanPayment);
	}

	@Transactional(readOnly = true)
	public LoanPaymentListItem getLoanPaymentListItem(Long id) {
		LoanPayment loanPayment = loanPaymentRepository.findOne(id);

		return loanPaymentListItemConverter.toDTO(loanPayment);
	}
}