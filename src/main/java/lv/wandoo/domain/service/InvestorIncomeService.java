package lv.wandoo.domain.service;

import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import lv.wandoo.domain.model.LoanPayment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

import static lv.wandoo.util.BigDecimalUtils.MONEY_ROUNDING;
import static lv.wandoo.util.BigDecimalUtils.MONEY_SCALE;

@Service
public class InvestorIncomeService {
	void allocateInvestorsMoney(LoanPayment loanPayment) {
		Loan loan = loanPayment.getLoan();
		Set<LoanInvestment> investments = loan.getInvestments();

		investments.forEach(investment -> {
			Investor investor = investment.getInvestor();
			BigDecimal partPercentage = investment.getAmount()
					.divide(loan.getAmount(), MONEY_SCALE, MONEY_ROUNDING);

			BigDecimal investorIncome
					= loanPayment.getTotal().multiply(partPercentage);

			investor.addFundsAvailable(investorIncome);
		});
	}
}
