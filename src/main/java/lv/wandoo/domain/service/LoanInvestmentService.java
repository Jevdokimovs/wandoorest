package lv.wandoo.domain.service;

import lv.wandoo.domain.dto.investment.Investment;
import lv.wandoo.domain.dto.investment.InvestmentConverter;
import lv.wandoo.domain.dto.investment.InvestorMoneyAsset;
import lv.wandoo.domain.dto.investment.InvestorMoneyAssetConverter;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import lv.wandoo.domain.repository.InvestorRepository;
import lv.wandoo.domain.repository.LoanInvestmentRepository;
import lv.wandoo.domain.repository.LoanRepository;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Comparator.comparing;

@Service
public class LoanInvestmentService {
	private LoanRepository loanRepository;
	private InvestorRepository investorRepository;
	private LoanInvestmentRepository loanInvestmentRepository;
	private InvestmentConverter investmentConverter;
	private InvestorMoneyAssetConverter investorMoneyAssetConverter;

	@Autowired
	public LoanInvestmentService(
			LoanRepository loanRepository,
			InvestorRepository investorRepository,
			LoanInvestmentRepository loanInvestmentRepository,
			InvestmentConverter investmentConverter,
			InvestorMoneyAssetConverter investorMoneyAssetConverter
	) {
		this.loanRepository = loanRepository;
		this.investorRepository = investorRepository;
		this.loanInvestmentRepository = loanInvestmentRepository;
		this.investmentConverter = investmentConverter;
		this.investorMoneyAssetConverter = investorMoneyAssetConverter;
	}

	@Transactional
	public void investToLoan(Investment investment) {
		LoanInvestment loanInvestment = investmentConverter.toEntity(investment);
		loanInvestmentRepository.persist(loanInvestment);

		Investor investor = loanInvestment.getInvestor();
		investor.withdrawFundsAvailable(investment.getAmount());
		investorRepository.persist(investor);
	}

	@Transactional(readOnly = true)
	public boolean isInvestmentAllowed(Long loanId, BigDecimal investAmount) {
		Loan loan = loanRepository.findOne(loanId);

		if (!loan.investmentAllowed()) {
			return false;
		}
		BigDecimal investmentAmountAvailable = loan.availableToInvest();

		return BigDecimalUtils.isGreatOrEquals(
				investmentAmountAvailable,
				investAmount
		);
	}

	@Transactional(readOnly = true)
	public InvestorMoneyAsset getInvestorMoneyAsset(Long loanInvestmentId) {
		LoanInvestment loanInvestment = loanInvestmentRepository.findOne(loanInvestmentId);

		return investorMoneyAssetConverter.toDTO(loanInvestment);
	}

	@Transactional(readOnly = true)
	public List<InvestorMoneyAsset> getInvestorMoneyAssetsByInvestor(Long investorId) {
		Investor investor = investorRepository.findOne(investorId);
		List<LoanInvestment> investments = newArrayList(investor.getInvestments())
				.stream()
				.sorted(comparing(LoanInvestment::getCreated).reversed())
				.collect(Collectors.toList());

		return investorMoneyAssetConverter.toDTOList(investments);
	}
}
