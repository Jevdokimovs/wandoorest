package lv.wandoo.domain.service;

import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.dto.loan.ImportLoanConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class LoanImportService {
	private LoanRepository loanRepository;
	private ImportLoanConverter importLoanConverter;

	@Autowired
	public LoanImportService(
			LoanRepository loanRepository,
			ImportLoanConverter importLoanConverter
	) {
		this.loanRepository = loanRepository;
		this.importLoanConverter = importLoanConverter;
	}

	@Transactional(readOnly = true)
	public Optional<Loan> getImportedLoan(
			String importSourceCode,
			String importLoanCode
	) {
		return loanRepository.getImportedLoan(
				importSourceCode,
				importLoanCode
		);
	}

	@Transactional(readOnly = true)
	public boolean isLoanAlreadyImported(
			String importSourceCode,
			String importLoanCode
	) {
		return loanRepository.getImportedLoan(
				importSourceCode,
				importLoanCode
		).isPresent();
	}

	
	@Transactional
	public void importLoan(ImportLoan importLoan) {
		Loan loan = importLoanConverter.toEntity(importLoan);
		loanRepository.persist(loan);
	}
}
