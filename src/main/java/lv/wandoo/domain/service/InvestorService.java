package lv.wandoo.domain.service;

import lv.wandoo.domain.dto.investor.InvestorProfile;
import lv.wandoo.domain.dto.investor.InvestorProfileConverter;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.dto.investor.InvestorRegistrationConverter;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.dto.investor.balance.InvestorBalanceConverter;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChange;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.value.ClientBalance;
import lv.wandoo.domain.repository.InvestorRepository;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.Comparator.comparing;

@Service
public class InvestorService {
	private InvestorRepository investorRepository;
	private InvestorRegistrationConverter investorRegistrationConverter;
	private InvestorBalanceConverter investorBalanceConverter;
	private InvestorProfileConverter investorProfileConverter;

	@Autowired
	public InvestorService(
			InvestorRepository investorRepository,
			InvestorRegistrationConverter investorRegistrationConverter,
			InvestorBalanceConverter investorBalanceConverter,
			InvestorProfileConverter investorProfileConverter
	) {
		this.investorRepository = investorRepository;
		this.investorRegistrationConverter = investorRegistrationConverter;
		this.investorBalanceConverter = investorBalanceConverter;
		this.investorProfileConverter = investorProfileConverter;
	}

	@Transactional
	public void register(InvestorRegistration investorRegistration) {
		Investor investor = investorRegistrationConverter.toEntity(investorRegistration);
		investorRepository.persist(investor);
	}

	@Transactional(readOnly = true)
	public boolean emailReserved(String email) {
		return investorRepository.findByEmail(email).isPresent();
	}

	@Transactional(readOnly = true)
	public boolean hasFundsToInvest(Long investorId, BigDecimal investAmount) {
		Investor investor = investorRepository.findOne(investorId);
		Optional<ClientBalance> clientBalanceOptional = investor.getClientBalance();
		if (!clientBalanceOptional.isPresent()) {
			return false;
		}

		BigDecimal fundsAvailable = clientBalanceOptional.get().getAvailable();
		return BigDecimalUtils.isGreatOrEquals(
				fundsAvailable,
				investAmount
		);
	}

	@Transactional(readOnly = true)
	public boolean exists(Long investorId) {
		return investorRepository.exists(investorId);
	}

	@Transactional
	public void addFunds(InvestorFundsChange investorFundsChange) {
		Investor investor = investorRepository
				.findOne(investorFundsChange.getInvestorId());
		investor.addFundsAvailable(investorFundsChange.getAmount());
		investorRepository.persist(investor);
	}

	@Transactional(readOnly = true)
	public InvestorBalance getBalance(Long investorId) {
		Investor investor = investorRepository.findOne(investorId);

		return investorBalanceConverter.toDTO(investor);
	}

	@Transactional(readOnly = true)
	public InvestorProfile getInvestorProfile(Long investorId) {
		Investor investor = investorRepository.findOne(investorId);

		return investorProfileConverter.toDTO(investor);
	}

	@Transactional(readOnly = true)
	public List<Investor> getInvestorsFullList(){
		return investorRepository.findAll();
	}

	@Transactional(readOnly = true)
	public InvestorProfile getAuthorizedInvestor() {
		//Hack, here we doesn't have authorization implemented,
		//will try return first persisted investor from list(Demo data required)

		Optional<Investor> investorAuthorized = investorRepository.findAll()
			.stream()
			.sorted(comparing(Investor::getCreated).reversed())
			.findFirst();

		return investorAuthorized.map(investor -> 
				investorProfileConverter.toDTO(investor)
		).orElseThrow(IllegalStateException::new);
	}
}