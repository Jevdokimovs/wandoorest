package lv.wandoo.domain.service;

import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.dto.loan.InvestmentLoanConverter;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

@Service
public class LoanService {
	private LoanRepository loanRepository;
	private InvestmentLoanConverter investmentLoanConverter;

	@Autowired
	public LoanService(
			LoanRepository loanRepository,
			InvestmentLoanConverter investmentLoanConverter
	) {
		this.loanRepository = loanRepository;
		this.investmentLoanConverter = investmentLoanConverter;
	}

	@Transactional(readOnly = true)
	public List<InvestmentLoan> getInvestmentReadyLoans() {
		List<Loan> list = loanRepository.findAll()
				.stream()
				.filter(Loan::getPublished)
				.sorted(comparing(Loan::getCreated).reversed())
				.collect(Collectors.toList());

		return investmentLoanConverter.toDTOList(list);
	}

	@Transactional(readOnly = true)
	public boolean exists(Long loanId) {
		return loanRepository.exists(loanId);
	}

	@Transactional(readOnly = true)
	public InvestmentLoan getInvestmentLoan(Long loanId) {
		Loan loan = loanRepository.findOne(loanId);

		return investmentLoanConverter.toDTO(loan);
	}
}
