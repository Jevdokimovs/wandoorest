package lv.wandoo.domain.validation;

import lv.wandoo.domain.dto.DTO;

public interface Validator<D extends DTO> {
	ValidationResult validate(D dto);
}
