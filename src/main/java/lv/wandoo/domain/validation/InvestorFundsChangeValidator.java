package lv.wandoo.domain.validation;

import lv.wandoo.domain.dto.investor.balance.InvestorFundsChange;
import lv.wandoo.domain.service.InvestorService;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvestorFundsChangeValidator implements Validator<InvestorFundsChange> {
	private InvestorService investorService;

	@Autowired
	public InvestorFundsChangeValidator(
			InvestorService investorService
	) {
		this.investorService = investorService;
	}

	@Override
	public ValidationResult validate(InvestorFundsChange investorFundsChange) {
		ValidationResult result = new ValidationResult();

		if (investorFundsChange == null) {
			result.addError(ValidationErrorConstants.INTERNAL_ERROR);
			return result;
		}

		if (!investorService.exists(investorFundsChange.getInvestorId())) {
			result.addError(ValidationErrorConstants.NOT_FOUND);
			return result;
		}

		if (!BigDecimalUtils.isPositive(investorFundsChange.getAmount())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		return result;
	}
}
