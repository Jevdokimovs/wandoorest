package lv.wandoo.domain.validation;

import com.google.common.base.Strings;
import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.service.LoanImportService;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImportLoanValidator implements Validator<ImportLoan> {
	private LoanImportService loanImportService;

	@Autowired
	public ImportLoanValidator(LoanImportService loanImportService) {
		this.loanImportService = loanImportService;
	}

	@Override
	public ValidationResult validate(ImportLoan importLoan) {
		ValidationResult result = new ValidationResult();

		if (importLoan == null) {
			result.addError(ValidationErrorConstants.INTERNAL_ERROR);
			return result;
		}

		if (Strings.isNullOrEmpty(importLoan.getImportSourceCode())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (Strings.isNullOrEmpty(importLoan.getImportLoanCode())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getBorrowerDetails() == null) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (Strings.isNullOrEmpty(importLoan.getBorrowerDetails().getIdentificationNumber())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getBorrowerDetails().getPersonType() == null) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getTermUnit() == null) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getTerm() <= 0) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getInterestRate() <= 0) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!BigDecimalUtils.isPositive(importLoan.getAmount())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoan.getInterestRate() <= 0) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (loanImportService.isLoanAlreadyImported(
				importLoan.getImportSourceCode(),
				importLoan.getImportLoanCode())
				) {
			result.addError(ValidationErrorConstants.ALREADY_PRESENT);
			return result;
		}

		return result;
	}
}
