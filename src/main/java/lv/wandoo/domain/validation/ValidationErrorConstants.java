package lv.wandoo.domain.validation;

class ValidationErrorConstants {
	static String INTERNAL_ERROR = "INTERNAL_ERROR";
	static String VALIDATION_FAILED = "VALIDATION_FAILED";
	static String NOT_FOUND = "NOT_FOUND";
	static String ALREADY_PRESENT = "ALREADY_PRESENT";
	static String INVESTMENT_DENIED = "INVESTMENT_DENIED";
	static String INSUFFICIENT_FUNDS = "INSUFFICIENT_FUNDS";
	static String UNIQUE_CHECK_NOT_PASSED = "UNIQUE_CHECK_NOT_PASSED";
}
