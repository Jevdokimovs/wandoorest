package lv.wandoo.domain.validation;

import com.google.common.base.Strings;
import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.service.LoanImportService;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ImportLoanPaymentValidator implements Validator<ImportLoanPayment> {
	private LoanImportService loanImportService;

	@Autowired
	public ImportLoanPaymentValidator(LoanImportService loanImportService) {
		this.loanImportService = loanImportService;
	}

	@Override
	public ValidationResult validate(ImportLoanPayment importLoanPayment) {
		ValidationResult result = new ValidationResult();

		if (Strings.isNullOrEmpty(importLoanPayment.getImportSourceCode())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (Strings.isNullOrEmpty(importLoanPayment.getImportLoanCode())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (importLoanPayment.getPaymentDate() == null) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!loanImportService.isLoanAlreadyImported(
				importLoanPayment.getImportSourceCode(),
				importLoanPayment.getImportLoanCode())
				) {
			result.addError(ValidationErrorConstants.NOT_FOUND);
			return result;
		}

		if (!BigDecimalUtils.isGreatOrEquals(
				importLoanPayment.getPrincipal(),
				BigDecimal.ZERO)
				) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!BigDecimalUtils.isGreatOrEquals(
				importLoanPayment.getInterest(),
				BigDecimal.ZERO)
				) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!BigDecimalUtils.isGreatOrEquals(
				importLoanPayment.getPenalty(),
				BigDecimal.ZERO)
				) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!BigDecimalUtils.isGreatOrEquals(
				importLoanPayment.getTotal(),
				BigDecimal.ZERO)
				) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!isTotalValid(importLoanPayment)) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		return result;
	}

	private boolean isTotalValid(ImportLoanPayment importLoanPayment) {
		BigDecimal sum = importLoanPayment.getPrincipal()
				.add(importLoanPayment.getInterest())
				.add(importLoanPayment.getPenalty());

		return BigDecimalUtils.isEqual(sum, importLoanPayment.getTotal());
	}
}
