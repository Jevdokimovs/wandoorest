package lv.wandoo.domain.validation;

import com.google.common.base.Strings;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.service.InvestorService;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvestorRegistrationValidator implements Validator<InvestorRegistration> {
	private static final int EMAIL_MAX_LENGTH = 150;
	private static final int PASSWORD_MAX_LENGTH = 60;

	private InvestorService investorService;

	@Autowired
	public InvestorRegistrationValidator(InvestorService investorService) {
		this.investorService = investorService;
	}

	@Override
	public ValidationResult validate(InvestorRegistration investorRegistration) {
		ValidationResult result = new ValidationResult();

		if (investorRegistration == null) {
			result.addError(ValidationErrorConstants.INTERNAL_ERROR);
			return result;
		}

		if (
				(!emailValid(investorRegistration.getEmail())) ||
						(!passwordValid(investorRegistration.getPassword()))
				) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (emailReserved(investorRegistration.getEmail())) {
			result.addError(ValidationErrorConstants.UNIQUE_CHECK_NOT_PASSED);
			return result;
		}

		return result;
	}

	private boolean emailValid(String str) {
		return (!Strings.isNullOrEmpty(str))
				&& str.length() <= EMAIL_MAX_LENGTH
				&& EmailValidator.getInstance().isValid(str);
	}

	private boolean passwordValid(String str) {
		return (!Strings.isNullOrEmpty(str))
				&& str.length() <= PASSWORD_MAX_LENGTH;
	}

	private boolean emailReserved(String email) {
		return investorService.emailReserved(email);
	}
}
