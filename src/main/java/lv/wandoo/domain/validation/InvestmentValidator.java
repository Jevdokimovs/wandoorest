package lv.wandoo.domain.validation;

import lv.wandoo.domain.dto.investment.Investment;
import lv.wandoo.domain.service.InvestorService;
import lv.wandoo.domain.service.LoanInvestmentService;
import lv.wandoo.domain.service.LoanService;
import lv.wandoo.util.BigDecimalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvestmentValidator implements Validator<Investment> {
	private LoanService loanService;
	private LoanInvestmentService loanInvestmentService;
	private InvestorService investorService;

	@Autowired
	public InvestmentValidator(
			LoanService loanService,
			LoanInvestmentService loanInvestmentService,
			InvestorService investorService
	) {
		this.loanService = loanService;
		this.loanInvestmentService = loanInvestmentService;
		this.investorService = investorService;
	}

	@Override
	public ValidationResult validate(Investment investment) {
		ValidationResult result = new ValidationResult();

		if (investment == null) {
			result.addError(ValidationErrorConstants.INTERNAL_ERROR);
			return result;
		}

		if (!BigDecimalUtils.isPositive(investment.getAmount())) {
			result.addError(ValidationErrorConstants.VALIDATION_FAILED);
			return result;
		}

		if (!investorService.exists(investment.getInvestorId())) {
			result.addError(ValidationErrorConstants.NOT_FOUND);
			return result;
		}

		if (!loanService.exists(investment.getLoanId())) {
			result.addError(ValidationErrorConstants.NOT_FOUND);
			return result;
		}

		if (!investorService.hasFundsToInvest(
				investment.getInvestorId(),
				investment.getAmount())
				) {
			result.addError(ValidationErrorConstants.INSUFFICIENT_FUNDS);
			return result;
		}

		if (!loanInvestmentService.isInvestmentAllowed(
				investment.getLoanId(),
				investment.getAmount())
				) {
			result.addError(ValidationErrorConstants.INVESTMENT_DENIED);
			return result;
		}

		return result;
	}
}
