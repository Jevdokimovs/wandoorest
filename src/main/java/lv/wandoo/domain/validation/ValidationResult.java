package lv.wandoo.domain.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

public class ValidationResult {
	private LinkedHashSet<String> errors;

	public ValidationResult() {
		errors = new LinkedHashSet<>();
	}

	public boolean hasErrors() {
		return (!errors.isEmpty());
	}

	public void addError(String error) {
		errors.add(error);
	}

	@JsonIgnore
	public Optional<String> getFirstError() {
		if (errors.isEmpty()) {
			return Optional.empty();
		} else {
			String errorsArr[] = errors.toArray(new String[0]);
			return Optional.ofNullable(errorsArr[errorsArr.length - 1]);
		}
	}

	public Set<String> getErrors() {
		return errors;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("errors", errors)
				.toString();
	}
}
