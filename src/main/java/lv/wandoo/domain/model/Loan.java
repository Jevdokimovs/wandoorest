package lv.wandoo.domain.model;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.model.common.BaseEntity;
import lv.wandoo.domain.model.value.BorrowerDetails;
import lv.wandoo.domain.model.value.TermUnit;
import lv.wandoo.util.BigDecimalUtils;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "loan")
public class Loan extends BaseEntity {
	@Type(type = "lv.wandoo.domain.model.persistence.BorrowerDetailsColumnTypeJSON")
	@Column(name = "borrower_details", columnDefinition = "json")
	private BorrowerDetails borrowerDetails;

	@Column(name = "issue_date", nullable = false, columnDefinition = "Date")
	private LocalDate issueDate;

	@Basic
	@Column(name = "term", nullable = false)
	private Integer term;

	@Enumerated(EnumType.STRING)
	@Column(name = "term_unit", nullable = false, length = 25)
	private TermUnit termUnit;

	@Basic
	@Column(name = "amount", nullable = false, precision = 10, scale = 2)
	private BigDecimal amount;

	@Basic
	@Column(name = "interest_rate", nullable = false, precision = 10, scale = 2)
	private Float interestRate;

	@Basic
	@Column(name = "country_code", nullable = false, length = 2)
	private String countryCode;

	@Basic
	@Column(
			name = "published",
			nullable = false,
			length = 1,
			columnDefinition = "TINYINT default 1"
	)
	private Boolean published = true;

	@Basic
	@Column(name = "import_source_code", length = 50)
	private String importSourceCode;

	@Basic
	@Column(name = "import_loan_code", length = 50)
	private String importLoanCode;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "loan")
	private Set<LoanInvestment> investments;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "loan")
	private Set<LoanPayment> payments;

	public Optional<BorrowerDetails> getBorrowerDetails() {
		return Optional.ofNullable(borrowerDetails);
	}

	public void setBorrowerDetails(BorrowerDetails borrowerDetails) {
		this.borrowerDetails = borrowerDetails;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public TermUnit getTermUnit() {
		return termUnit;
	}

	public void setTermUnit(TermUnit termUnit) {
		this.termUnit = termUnit;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public Optional<String> getImportSourceCode() {
		return Optional.ofNullable(importSourceCode);
	}

	public void setImportSourceCode(String importSourceCode) {
		this.importSourceCode = importSourceCode;
	}

	public Optional<String> getImportLoanCode() {
		return Optional.ofNullable(importLoanCode);
	}

	public void setImportLoanCode(String importLoanCode) {
		this.importLoanCode = importLoanCode;
	}

	public Set<LoanInvestment> getInvestments() {
		return investments;
	}

	public Set<LoanPayment> getPayments() {
		return payments;
	}

	public BigDecimal investedTotal() {
		return getInvestments().stream()
				.map(LoanInvestment::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public BigDecimal availableToInvest() {
		return getAmount().subtract(this.investedTotal());
	}

	public boolean investmentAllowed() {
		return BigDecimalUtils.isPositive(this.availableToInvest());
	}

	@Override
	public String toString() {
		return MoreObjects
				.toStringHelper(this)
				.add("id", super.getId())
				.add("borrowerDetails", borrowerDetails)
				.add("issueDate", issueDate)
				.add("term", term)
				.add("termUnit", termUnit)
				.add("amount", amount)
				.add("interestRate", interestRate)
				.add("countryCode", countryCode)
				.add("published", published)
				.toString();
	}
}
