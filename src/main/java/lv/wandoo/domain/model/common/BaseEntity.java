package lv.wandoo.domain.model.common;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Generated(GenerationTime.INSERT)
	@Column(
			name = "created",
			nullable = false,
			updatable = false,
			columnDefinition = "Timestamp DEFAULT CURRENT_TIMESTAMP"
	)
	private LocalDateTime created;

	@Generated(GenerationTime.ALWAYS)
	@Column(
			name = "updated",
			nullable = false,
			columnDefinition = "Timestamp ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
	)
	private LocalDateTime updated;

	public BaseEntity() {
	}

	BaseEntity(Long id, LocalDateTime created, LocalDateTime mdate, Boolean active) {
		this.id = id;
		this.created = created;
		this.updated = mdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BaseEntity)) return false;
		BaseEntity that = (BaseEntity) o;
		return Objects.equal(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.toString();
	}
}