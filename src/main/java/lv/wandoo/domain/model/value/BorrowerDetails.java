package lv.wandoo.domain.model.value;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class BorrowerDetails {
	private String identificationNumber;
	private PersonType personType;
	private String fullName;
	private String companyName;

	public BorrowerDetails() {
	}

	public BorrowerDetails(
			String identificationNumber, 
			PersonType personType, 
			String fullName, 
			String companyName
	) {
		this.identificationNumber = identificationNumber;
		this.personType = personType;
		this.fullName = fullName;
		this.companyName = companyName;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identeficationNumber) {
		this.identificationNumber = identeficationNumber;
	}

	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(PersonType personType) {
		this.personType = personType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BorrowerDetails)) return false;
		BorrowerDetails that = (BorrowerDetails) o;
		return Objects.equal(identificationNumber, that.identificationNumber) &&
				personType == that.personType;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(identificationNumber, personType);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("identificationNumber", identificationNumber)
				.add("personType", personType)
				.add("fullName", fullName)
				.add("companyName", companyName)
				.toString();
	}
}
