package lv.wandoo.domain.model.value;

public class BorrowerDetailsBuilder {
	private String identificationNumber;
	private PersonType personType;
	private String fullName;
	private String companyName;

	public BorrowerDetailsBuilder identificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
		return this;
	}

	public BorrowerDetailsBuilder personType(PersonType personType) {
		this.personType = personType;
		return this;
	}

	public BorrowerDetailsBuilder fullName(String fullName) {
		this.fullName = fullName;
		return this;
	}

	public BorrowerDetailsBuilder companyName(String companyName) {
		this.companyName = companyName;
		return this;
	}

	public BorrowerDetails build() {
		return new BorrowerDetails(identificationNumber, personType, fullName, companyName);
	}
}