package lv.wandoo.domain.model.value;

public enum TermUnit {
	DAY, MONTH, YEAR
}
