package lv.wandoo.domain.model.value;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;

public class ClientBalance {
	private BigDecimal available;

	public BigDecimal getAvailable() {
		return available;
	}

	public void setAvailable(BigDecimal available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("available", available)
				.toString();
	}

	public static class BalaceInitializer {
		public static ClientBalance investorDefaultBalance() {
			ClientBalance clientBalance = new ClientBalance();
			clientBalance.setAvailable(BigDecimal.ZERO);
			return clientBalance;
		}
	}
}
