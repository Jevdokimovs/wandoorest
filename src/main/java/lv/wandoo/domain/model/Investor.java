package lv.wandoo.domain.model;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.model.common.BaseEntity;
import lv.wandoo.domain.model.value.ClientBalance;
import lv.wandoo.domain.model.value.PersonType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "investor")
public class Investor extends BaseEntity {
	@Basic
	@Column(name = "email", nullable = false, unique = true, length = 150)
	private String email;

	@Basic
	@Column(name = "password", nullable = false, length = 60)
	private String password;

	@Basic
	@Column(name = "hash", nullable = false, unique = true, length = 32)
	private String hash;

	@Basic
	@Column(name = "active", nullable = false, columnDefinition = "TINYINT default 1", length = 1)
	private Boolean active = true;

	@Enumerated(EnumType.STRING)
	@Column(name = "person_type", nullable = false, length = 100)
	private PersonType personType;

	@Basic
	@Column(
			name = "country_code",
			nullable = false,
			length = 2
	)
	private String countryCode;

	@Type(type = "lv.wandoo.domain.model.persistence.ClientBalanceColumnTypeJSON")
	@Column(
			name = "client_balance",
			columnDefinition = "json"
	)
	private ClientBalance clientBalance
			= ClientBalance.BalaceInitializer.investorDefaultBalance();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "investor")
	private Set<LoanInvestment> investments;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(PersonType personType) {
		this.personType = personType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Optional<ClientBalance> getClientBalance() {
		return Optional.ofNullable(clientBalance);
	}

	public void setClientBalance(ClientBalance clientBalance) {
		this.clientBalance = clientBalance;
	}

	public Set<LoanInvestment> getInvestments() {
		return investments;
	}

	public void withdrawFundsAvailable(BigDecimal amount) {
		initDefaultClientBalanceIfNotPresent();

		BigDecimal fundsAvailable = clientBalance.getAvailable();
		clientBalance.setAvailable(fundsAvailable.subtract(amount));
	}

	public void addFundsAvailable(BigDecimal amount) {
		initDefaultClientBalanceIfNotPresent();

		BigDecimal fundsAvailable = clientBalance.getAvailable();
		clientBalance.setAvailable(fundsAvailable.add(amount));
	}

	private void initDefaultClientBalanceIfNotPresent() {
		if(!getClientBalance().isPresent()) {
			clientBalance = ClientBalance.BalaceInitializer.investorDefaultBalance();
		}
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", super.getId())
				.add("email", email)
				.add("password", password)
				.add("hash", hash)
				.add("active", active)
				.add("personType", personType)
				.add("countryCode", countryCode)
				.toString();
	}
}
