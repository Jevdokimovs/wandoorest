package lv.wandoo.domain.model.persistence;

import lv.wandoo.domain.model.value.BorrowerDetails;

@SuppressWarnings("unused")
public class BorrowerDetailsColumnTypeJSON extends AbstractColumnTypeJSON {
	@Override
	public Class returnedClass() {
		return BorrowerDetails.class;
	}
}
