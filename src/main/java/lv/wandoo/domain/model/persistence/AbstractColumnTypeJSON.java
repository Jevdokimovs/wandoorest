package lv.wandoo.domain.model.persistence;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.SimpleType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public abstract class AbstractColumnTypeJSON implements UserType {
	private static final int[] SQL_TYPES = {Types.CHAR};

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public void nullSafeSet(
			PreparedStatement st,
			Object value,
			int index,
			SessionImplementor session
	) throws HibernateException, SQLException {
		if (value == null) {
			st.setString(index, null);
		} else {
			st.setString(index, convertObjectToJson(value));
		}
	}

	@Override
	public Object nullSafeGet(
			ResultSet rs,
			String[] names,
			SessionImplementor session,
			Object owner
	) throws HibernateException, SQLException {
		String content = rs.getString(names[0]);

		if (content != null) {
			return convertJsonToObject(content);
		}
		return null;
	}

	private Object convertJsonToObject(String content) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JavaType type = createJavaType(mapper);

			return mapper.readValue(content, type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String convertObjectToJson(Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
			return mapper.writeValueAsString(object);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		String json = convertObjectToJson(value);
		return convertJsonToObject(json);
	}

	@Override
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return deepCopy(original);
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) deepCopy(value);
	}

	@Override
	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return deepCopy(cached);
	}

	@SuppressWarnings({"unused", "deprecation"})
	private JavaType createJavaType(ObjectMapper mapper) {
		return SimpleType.construct(returnedClass());
	}

	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return x == y
				||
				!(x == null || y == null) && x.equals(y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		return null == x ? 0 : x.hashCode();
	}
}