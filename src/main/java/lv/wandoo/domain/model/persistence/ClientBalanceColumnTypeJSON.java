package lv.wandoo.domain.model.persistence;

import lv.wandoo.domain.model.value.ClientBalance;

@SuppressWarnings("unused")
public class ClientBalanceColumnTypeJSON extends AbstractColumnTypeJSON {
	@Override
	public Class returnedClass() {
		return ClientBalance.class;
	}
}
