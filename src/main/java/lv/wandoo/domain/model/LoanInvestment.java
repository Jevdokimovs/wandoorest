package lv.wandoo.domain.model;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.model.common.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "loan_investment")
public class LoanInvestment extends BaseEntity {
	@ManyToOne
	@JoinColumn(name = "loan_id", referencedColumnName = "id", nullable = false)
	private Loan loan;

	@ManyToOne
	@JoinColumn(name = "investor_id", referencedColumnName = "id", nullable = false)
	private Investor investor;

	@Basic
	@Column(name = "amount", nullable = false, precision = 10, scale = 2)
	private BigDecimal amount;

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public Investor getInvestor() {
		return investor;
	}

	public void setInvestor(Investor investor) {
		this.investor = investor;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", super.getId())
				.add("loan", loan)
				.add("investor", investor)
				.add("amount", amount)
				.toString();
	}
}
