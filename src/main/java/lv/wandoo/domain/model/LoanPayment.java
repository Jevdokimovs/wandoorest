package lv.wandoo.domain.model;

import com.google.common.base.MoreObjects;
import lv.wandoo.domain.model.common.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "loan_payment")
public class LoanPayment extends BaseEntity {
	@ManyToOne
	@JoinColumn(name = "loan_id", referencedColumnName = "id", nullable = false)
	private Loan loan;

	@Column(name = "payment_date", nullable = false, columnDefinition = "Date")
	private LocalDate paymentDate;

	@Basic
	@Column(name = "principal", nullable = false, precision = 10, scale = 2)
	private BigDecimal principal;

	@Basic
	@Column(name = "interest", nullable = false, precision = 10, scale = 2)
	private BigDecimal interest;

	@Basic
	@Column(name = "penalty", nullable = false, precision = 10, scale = 2)
	private BigDecimal penalty = BigDecimal.ZERO;

	@Basic
	@Column(name = "total", nullable = false, precision = 10, scale = 2)
	private BigDecimal total;

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getPenalty() {
		return penalty;
	}

	public void setPenalty(BigDecimal penalty) {
		this.penalty = penalty;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", super.getId())
				.add("loan", loan)
				.add("paymentDate", paymentDate)
				.add("principal", principal)
				.add("interest", interest)
				.add("penalty", penalty)
				.add("total", total)
				.toString();
	}
}
