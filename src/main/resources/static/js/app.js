var BASE_WS_URL = '/ws/';
var investorId;
var investorBalance;

$(document).ready(function () {
	$.views.helpers({
		formatCurrency: function (val) {
			return val.toFixed(2) + " &euro;";
		},
		lowerCase: function (val) {
			return val.toLowerCase();
		}
	});

	reloadLoansAvailable();
	loadAuthorizedInvestorData();
});

$(document).on("input", '.currencyInput', function () {
	var value = $(this).val();
	var availableToInvest = parseFloat($(this).attr("data-availableToInvest"));

	$(this).val(validateCurrency(value, availableToInvest));
});

function validateCurrency(value, maxAmount) {
	var validatedValue = value.replace(/[^0-9,\.]/, '');
	if (value !== validatedValue) {
		value = validatedValue;
	}

	if ((value.substring(value.indexOf('.')).length > 3) &&
		(value.indexOf('.') > -1)
	) {
		value = value.substring(0, value.indexOf('.') + 3);
	}

	if ($.isNumeric(value) && value > maxAmount) {
		value = maxAmount
	}

	return value;
}

$(document).on('click', '.invest-button', function () {
	var loanId = $(this).attr("data-id");
	var amount = $('.invest-amount[data-id="' + loanId + '"]').val();

	var amountValid = $.isNumeric(amount) && parseFloat(amount) > 0;
	if (!amountValid) {
		alert('Investment amount: ' + amount + ' EUR is invalid amount value!');
		return true;
	}

	invest(loanId, amount);
});

function invest(loanId, amount) {
	$.ajax({
		type: 'POST',
		url: BASE_WS_URL + 'loan-investment/invest',
		data: '{ "loanId": ' + loanId + ', "investorId": ' + investorId + ', "amount": ' + amount + '}', // or JSON.stringify ({name: 'jonas'}),
		success: function (data) {
			if (data.errors.length == 0) {
				reloadLoansAvailable();
				loadAuthorizedInvestorData();
			} else {
				alert(data.errors);
			}
		},
		error: function (status, error) {
			alert('status: ' + status + ', error: ' + error);
		},
		contentType: "application/json",
		dataType: 'json'
	});
}

function reloadLoansAvailable() {
	$.getJSON(
		BASE_WS_URL + "loan/investment-ready-list",
		function (data) {
			$("#loanRows").html($("#loanRow-tmpl").render(data));
		});
}

function loadAuthorizedInvestorData() {
	$("#profile-summary").html('');
	$.getJSON(
		BASE_WS_URL + "investor/get-authorized",
		function (data) {
			$("#profile-summary").append($("#investor-profile-welcome-tmpl").render(data));
			investorId = data.id;
			getInvestorBalance(investorId);
			loadMyInvestments(investorId);
		});
}

function loadMyInvestments(investorId) {
	$.getJSON(
		BASE_WS_URL + "loan-investment/by-investor/" + investorId,
		function (data) {
			$("#myInvestmentsRows").html($("#myInvestmentRow-tmpl").render(data));
		});
}

function getInvestorBalance(investorId) {
	$.getJSON(
		BASE_WS_URL + "investor/" + investorId + "/balance",
		function (data) {
			$("#profile-summary").append($("#investor-profile-balance-tmpl").render(data));
			investorBalance = data.available;
		});
}