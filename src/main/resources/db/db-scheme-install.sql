CREATE DATABASE IF NOT EXISTS `wandoo`
  CHARACTER SET utf8
  COLLATE utf8_general_ci;
USE `wandoo`;

-- **Optional CLEANUP
DROP TABLE IF EXISTS `investor` CASCADE;
DROP TABLE IF EXISTS `loan` CASCADE;
DROP TABLE IF EXISTS `loan_investment` CASCADE;
DROP TABLE IF EXISTS `loan_payment` CASCADE;

CREATE TABLE `investor` (
  `id`             BIGINT(20) AUTO_INCREMENT NOT NULL,
  `created`        TIMESTAMP                 NOT NULL                             DEFAULT CURRENT_TIMESTAMP,
  `updated`        TIMESTAMP                 NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `active`         TINYINT(4)                NOT NULL                             DEFAULT '1',
  `country_code`   VARCHAR(2)
                   CHARACTER SET utf8
                   COLLATE utf8_general_ci   NOT NULL,
  `email`          VARCHAR(150)
                   CHARACTER SET utf8
                   COLLATE utf8_general_ci   NOT NULL,
  `hash`           VARCHAR(32)
                   CHARACTER SET utf8
                   COLLATE utf8_general_ci   NOT NULL,
  `password`       VARCHAR(60)
                   CHARACTER SET utf8
                   COLLATE utf8_general_ci   NOT NULL,
  `person_type`    VARCHAR(100)
                   CHARACTER SET utf8
                   COLLATE utf8_general_ci   NOT NULL,
  `client_balance` JSON                      NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `email` UNIQUE (`email`),
  CONSTRAINT `hash` UNIQUE (`hash`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci
  AUTO_INCREMENT = 1;

CREATE TABLE `loan` (
  `id`               BIGINT(20) AUTO_INCREMENT NOT NULL,
  `created`          TIMESTAMP                 NOT NULL                             DEFAULT CURRENT_TIMESTAMP,
  `updated`          TIMESTAMP                 NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `amount`           DECIMAL(10, 2)            NOT NULL,
  `borrower_details` JSON                      NULL,
  `country_code`     VARCHAR(2)
                     CHARACTER SET utf8
                     COLLATE utf8_general_ci   NOT NULL,
  `interest_rate`    FLOAT(12, 0)              NOT NULL,
  `issue_date`       DATE                      NOT NULL,
  `published`        TINYINT(4)                NOT NULL                             DEFAULT '1',
  `term`             INT(11)                   NOT NULL,
  `term_unit`        VARCHAR(25)
                     CHARACTER SET utf8
                     COLLATE utf8_general_ci   NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci
  AUTO_INCREMENT = 1;

CREATE TABLE `loan_investment` (
  `id`          BIGINT(20) AUTO_INCREMENT NOT NULL,
  `created`     TIMESTAMP                 NOT NULL                             DEFAULT CURRENT_TIMESTAMP,
  `updated`     TIMESTAMP                 NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `amount`      DECIMAL(10, 2)            NOT NULL,
  `investor_id` BIGINT(20)                NOT NULL,
  `loan_id`     BIGINT(20)                NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `loan_investment_loan_fk` FOREIGN KEY (`loan_id`) REFERENCES `loan` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `loan_investment_investor_fk` FOREIGN KEY (`investor_id`) REFERENCES `investor` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci
  AUTO_INCREMENT = 1;

CREATE INDEX `loan_investment_investor_idx` USING BTREE ON `loan_investment` (`investor_id`);
CREATE INDEX `loan_investment_loan_idx` USING BTREE ON `loan_investment` (`loan_id`);

CREATE TABLE `loan_payment` (
  `id`           BIGINT(20) AUTO_INCREMENT NOT NULL,
  `created`      TIMESTAMP                 NOT NULL                             DEFAULT CURRENT_TIMESTAMP,
  `updated`      TIMESTAMP                 NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `interest`     DECIMAL(10, 2)            NOT NULL,
  `payment_date` DATE                      NOT NULL,
  `penalty`      DECIMAL(10, 2)            NOT NULL,
  `principal`    DECIMAL(10, 2)            NOT NULL,
  `total`        DECIMAL(10, 2)            NOT NULL,
  `loan_id`      BIGINT(20)                NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `loan_payment_loan_fk` FOREIGN KEY (`loan_id`) REFERENCES `loan` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci
  AUTO_INCREMENT = 1;

CREATE INDEX `loan_payment_loan_idx` USING BTREE ON `loan_payment` (`loan_id`);