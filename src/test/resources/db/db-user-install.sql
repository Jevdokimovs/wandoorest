CREATE USER 'wandoo_test'@'%' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON wandoo_test.* TO 'wandoo_test'@'%';
FLUSH PRIVILEGES;