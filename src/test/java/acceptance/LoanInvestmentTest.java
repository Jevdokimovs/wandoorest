package acceptance;

import acceptance.common.BaseAcceptanceTest;
import lv.wandoo.domain.dto.investment.Investment;
import lv.wandoo.domain.dto.investment.InvestmentBuilder;
import lv.wandoo.domain.dto.investment.InvestorMoneyAsset;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import lv.wandoo.domain.validation.ValidationResult;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static lv.wandoo.util.BigDecimalUtils.moneyScaleFormat;
import static org.junit.Assert.*;

@Transactional
public class LoanInvestmentTest extends BaseAcceptanceTest {
	@Test
	public void doInvestmentToImportedLoan() {
		Investor investor = investorTestAPI
				.createTestInvestor("loan-investment-test@inbox.lv");
		Long investorId = investor.getId();
		assertNotNull(investor);

		BigDecimal importLoanAmount = new BigDecimal("1000");
		String importSourceCode = "lv_4finance";
		String importLoanCode = "1111111110";
		Loan loan = loanTestAPI.importTestLoan(importLoanAmount, importSourceCode, importLoanCode);
		Long loanId = loan.getId();
		assertNotNull(loan);

		BigDecimal investorFundsAmount = new BigDecimal("1000");
		BigDecimal investmentAmount = new BigDecimal("200");
		investorTestAPI.addFundsToInvestor(investorId, investorFundsAmount);

		Investment investment = new InvestmentBuilder()
				.investorId(investorId)
				.loanId(loanId)
				.amount(investmentAmount)
				.build();

		ResponseEntity<ValidationResult> responseEntity
				= loanInvestmentTestAPI.postLoanInvestmentData(investment);
		responseOK(responseEntity);
		validationPassed(responseEntity);

		Optional<LoanInvestment> loanInvestment =
		loanInvestmentTestAPI.getLastByLoanAndInvestor(loan, investor);
		assertTrue(loanInvestment.isPresent());
		Long loanInvestmentId = loanInvestment.get().getId();

		InvestorMoneyAsset investorMoneyAsset
				= loanInvestmentTestAPI.getInvestorMoneyAsset(loanInvestmentId);

		assertEquals(
				moneyScaleFormat(investorMoneyAsset.getAmount()),
				moneyScaleFormat(investmentAmount)
		);

		assertEquals(investorMoneyAsset.getLoanId(), loan.getId());
		assertEquals(investorMoneyAsset.getInterestRate(), loan.getInterestRate());
		assertEquals(investorMoneyAsset.getTerm(), loan.getTerm());
		assertEquals(investorMoneyAsset.getTermUnit(), loan.getTermUnit());

		InvestorBalance investorBalance =
				investorTestAPI.getInvestorBalance(investorId);
		BigDecimal investorAvailableAmountExpected
				= investorFundsAmount.subtract(investmentAmount);
		assertEquals(
				moneyScaleFormat(investorBalance.getAvailable()),
				moneyScaleFormat(investorAvailableAmountExpected)
		);

		InvestmentLoan investmentLoan = loanTestAPI.getInvestmentLoan(loanId);
		BigDecimal loanAvailableToInvestExpected
				= loan.getAmount().subtract(investorMoneyAsset.getAmount());
		assertEquals(
				moneyScaleFormat(investmentLoan.getAvailableToInvest()),
				moneyScaleFormat(loanAvailableToInvestExpected)
		);
	}
}
