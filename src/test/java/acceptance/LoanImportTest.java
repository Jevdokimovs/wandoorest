package acceptance;

import acceptance.api.LoanTestAPI;
import acceptance.common.BaseAcceptanceTest;
import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.validation.ValidationResult;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static lv.wandoo.util.BigDecimalUtils.moneyScaleFormat;
import static org.junit.Assert.*;

@Transactional
public class LoanImportTest extends BaseAcceptanceTest {
	@SuppressWarnings({"ConstantConditions"})
	@Test
	public void importLoan() {
		String importSourceCode = "lv_4finance";
		String importLoanCode = "1111111111";

		BigDecimal importLoanAmount = new BigDecimal("450");
		ImportLoan importLoan = LoanTestAPI.createTestImportLoan(
				importLoanAmount,
				importSourceCode,
				importLoanCode
		);

		ResponseEntity<ValidationResult> responseEntity
				= loanTestAPI.postLoanImportData(importLoan);
		responseOK(responseEntity);
		validationPassed(responseEntity);

		assertTrue(loanTestAPI.getLoanByImportCredentials(
				importSourceCode,
				importLoanCode
		).isPresent());
		Loan loan = loanTestAPI
				.getLoanByImportCredentials(importSourceCode, importLoanCode).get();
		assertTrue(
				loan.getImportSourceCode().get().equals(importLoan.getImportSourceCode())
		);
		assertTrue(
				loan.getImportLoanCode().get().equals(importLoan.getImportLoanCode())
		);

		Long loanId = loan.getId();
		InvestmentLoan investmentLoan = loanTestAPI.getInvestmentLoan(loanId);
		assertNotNull(investmentLoan);
		assertEquals(
				investmentLoan.getBorrowerTitle(),
				importLoan.getBorrowerDetails().getFullName()
		);
		assertEquals(
				moneyScaleFormat(investmentLoan.getAvailableToInvest()),
				moneyScaleFormat(importLoan.getAmount())
		);
		assertEquals(
				investmentLoan.getImportDate(),
				loan.getCreated().toLocalDate()
		);

		List<InvestmentLoan> investmentLoansList = loanTestAPI.getInvestmentReadyList();
		assertTrue(investmentLoansList.stream().anyMatch(
				item -> Objects.equals(item.getId(), investmentLoan.getId())
				)
		);
	}
}
