package acceptance;

import acceptance.api.LoanPaymentTestAPI;
import acceptance.common.BaseAcceptanceTest;
import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.dto.payment.LoanPaymentListItem;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanPayment;
import lv.wandoo.domain.validation.ValidationResult;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static lv.wandoo.util.BigDecimalUtils.moneyScaleFormat;
import static org.junit.Assert.*;

@Transactional
public class LoanPaymentImportTest extends BaseAcceptanceTest {
	@Test
	public void importLoanAndPayment() {
		String importSourceCode = "lv_opencredit";
		String importLoanCode = "1111111112";

		BigDecimal importLoanAmount = new BigDecimal("1200");
		Loan loan = loanTestAPI.importTestLoan(importLoanAmount, importSourceCode, importLoanCode);
		assertNotNull(loan);
		Long loanId = loan.getId();

		ImportLoanPayment importLoanPayment = LoanPaymentTestAPI.createImportLoanPayment(
				importSourceCode,
				importLoanCode,
				new BigDecimal("15"),
				new BigDecimal("15"),
				BigDecimal.ZERO
		);

		ResponseEntity<ValidationResult> responseEntity
				= loanPaymentTestAPI.postLoanPaymentData(importLoanPayment);

		assertTrue(responseOK(responseEntity));
		assertTrue(validationPassed(responseEntity));

		Optional<LoanPayment> loanPayment = loanPaymentTestAPI.getPaymentByLoanId(loanId);
		assertTrue(loanPayment.isPresent());
		Long loanPaymentId = loanPayment.get().getId();

		LoanPaymentListItem loanPaymentListItem
				= loanPaymentTestAPI.getLoanPaymentListItem(loanPaymentId);

		assertNotNull(loanPaymentListItem);

		assertEquals(loanPaymentListItem.getLoanId(), loanId);

		assertEquals(
				moneyScaleFormat(loanPaymentListItem.getPrincipal()),
				moneyScaleFormat(importLoanPayment.getPrincipal())
		);
		assertEquals(
				moneyScaleFormat(loanPaymentListItem.getInterest()),
				moneyScaleFormat(importLoanPayment.getInterest())
		);
		assertEquals(
				moneyScaleFormat(loanPaymentListItem.getPenalty()),
				moneyScaleFormat(importLoanPayment.getPenalty())
		);
		assertEquals(
				moneyScaleFormat(loanPaymentListItem.getTotal()),
				moneyScaleFormat(importLoanPayment.getTotal())
		);
	}
}
