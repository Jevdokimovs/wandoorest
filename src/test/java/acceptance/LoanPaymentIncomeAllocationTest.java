package acceptance;

import acceptance.api.LoanPaymentTestAPI;
import acceptance.common.BaseAcceptanceTest;
import lv.wandoo.domain.dto.investment.InvestmentBuilder;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Transactional
public class LoanPaymentIncomeAllocationTest extends BaseAcceptanceTest {
	@Test
	public void importPaymentAndAllocateInvestorMoney() {
		String importSourceCode = "lv_credit24";
		String importLoanCode = "1111111113";

		BigDecimal importLoanAmount = new BigDecimal("800");
		Loan loan = loanTestAPI.importTestLoan(importLoanAmount, importSourceCode, importLoanCode);
		assertNotNull(loan);
		Long loanId = loan.getId();

		Investor investor = investorTestAPI
				.createTestInvestor("loan-payment-allocation-test@inbox.lv");
		assertNotNull(investor);
		Long investorId = investor.getId();

		BigDecimal investorFundsInitial = new BigDecimal("1000");
		investorTestAPI.addFundsToInvestor(investorId, investorFundsInitial);

		BigDecimal investmentAmount = new BigDecimal("200");
		loanInvestmentTestAPI.postLoanInvestmentData(new InvestmentBuilder()
				.investorId(investorId)
				.loanId(loanId)
				.amount(investmentAmount)
				.build());

		Optional<LoanInvestment> loanInvestment = loanInvestmentTestAPI.getLastByLoanAndInvestor(
				loan, investor
		);
		assertTrue(loanInvestment.isPresent());

		ImportLoanPayment importLoanPayment = LoanPaymentTestAPI.createImportLoanPayment(
				importSourceCode,
				importLoanCode,
				new BigDecimal("20"),
				new BigDecimal("20"),
				new BigDecimal("20")
		);
		loanPaymentTestAPI.postLoanPaymentData(importLoanPayment);

		InvestorBalance investorBalance = investorTestAPI.getInvestorBalance(investorId);
		assertNotNull(investorBalance);

		assertEquals(investorBalance.getAvailable(), new BigDecimal("815"));
	}
}
