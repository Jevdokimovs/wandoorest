package acceptance.api;

import lv.wandoo.domain.dto.investor.InvestorProfile;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.dto.investor.InvestorRegistrationBuilder;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChange;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChangeBuilder;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.value.PersonType;
import lv.wandoo.domain.repository.InvestorRepository;
import lv.wandoo.domain.validation.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

import static acceptance.common.BaseAcceptanceTest.BASE_REST_URL;

@Component
public class InvestorTestAPI {
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private InvestorRepository investorRepository;

	public InvestorBalance getInvestorBalance(Long investorId) {
		return restTemplate.getForObject(
				BASE_REST_URL + "/investor/" + investorId + "/balance",
				InvestorBalance.class
		);
	}

	public ResponseEntity<ValidationResult> postFundsToInvestor(
			InvestorFundsChange dto
	) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<InvestorFundsChange> httpEntity = new HttpEntity<>(
				dto,
				headers
		);

		return restTemplate.exchange(
				BASE_REST_URL + "/back-office/investor/funds/add",
				HttpMethod.POST,
				httpEntity,
				ValidationResult.class
		);
	}

	public InvestorProfile getInvestorProfile(Long investorId) {
		return restTemplate.getForObject(
				BASE_REST_URL + "/investor/" + investorId,
				InvestorProfile.class
		);
	}

	public ResponseEntity<ValidationResult> postRegistrationData(
			InvestorRegistration dto
	) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<InvestorRegistration> httpEntity = new HttpEntity<>(
				dto,
				headers
		);

		return restTemplate.exchange(
				BASE_REST_URL + "/investor/register",
				HttpMethod.POST,
				httpEntity,
				ValidationResult.class
		);
	}

	public Optional<Investor> getPersistedByEmail(String email) {
		return investorRepository.findByEmail(email);
	}

	public static InvestorRegistration createTestInvestorRegistration(String email) {
		return new InvestorRegistrationBuilder()
				.email(email)
				.password("password")
				.personType(PersonType.PRIVATE)
				.countryCode("LV")
				.build();
	}

	@SuppressWarnings("SameParameterValue")
	public Investor createTestInvestor(String email) {
		InvestorRegistration investorRegistration
				= InvestorTestAPI.createTestInvestorRegistration(email);
		postRegistrationData(investorRegistration);

		return getPersistedByEmail(email).orElse(null);
	}

	public void addFundsToInvestor(Long investorId, BigDecimal addAmount) {
		postFundsToInvestor(new InvestorFundsChangeBuilder()
				.investorId(investorId)
				.amount(addAmount)
				.build()
		);
	}
}
