package acceptance.api;

import lv.wandoo.domain.dto.investment.Investment;
import lv.wandoo.domain.dto.investment.InvestorMoneyAsset;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.LoanInvestment;
import lv.wandoo.domain.repository.LoanInvestmentRepository;
import lv.wandoo.domain.validation.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static acceptance.common.BaseAcceptanceTest.BASE_REST_URL;
import static java.util.Comparator.comparing;

@Component
public class LoanInvestmentTestAPI {
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private LoanInvestmentRepository loanInvestmentRepository;

	public ResponseEntity<ValidationResult> postLoanInvestmentData(
			Investment dto
	) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Investment> httpEntity = new HttpEntity<>(
				dto,
				headers
		);

		return restTemplate.exchange(
				BASE_REST_URL + "/loan-investment/invest",
				HttpMethod.POST,
				httpEntity,
				ValidationResult.class
		);
	}

	public InvestorMoneyAsset getInvestorMoneyAsset(Long loanInvestmentId) {
		return restTemplate.getForObject(
				BASE_REST_URL + "/loan-investment/" + loanInvestmentId,
				InvestorMoneyAsset.class
		);
	}

	public Optional<LoanInvestment> getLastByLoanAndInvestor(Loan loan, Investor investor) {
		List<LoanInvestment> list = loanInvestmentRepository.getByInvestorAndLoan(
				loan,
				investor
		);

		return list
				.stream()
				.sorted(comparing(LoanInvestment::getCreated).reversed())
				.findFirst();
	}
}
