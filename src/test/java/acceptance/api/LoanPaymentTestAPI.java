package acceptance.api;

import lv.wandoo.domain.dto.payment.ImportLoanPayment;
import lv.wandoo.domain.dto.payment.ImportLoanPaymentBuilder;
import lv.wandoo.domain.dto.payment.LoanPaymentListItem;
import lv.wandoo.domain.model.LoanPayment;
import lv.wandoo.domain.repository.LoanPaymentRepository;
import lv.wandoo.domain.validation.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static acceptance.common.BaseAcceptanceTest.BASE_REST_URL;

@Component
public class LoanPaymentTestAPI {
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private LoanPaymentRepository loanPaymentRepository;

	public ResponseEntity<ValidationResult> postLoanPaymentData(
			ImportLoanPayment dto
	) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ImportLoanPayment> httpEntity = new HttpEntity<>(
				dto,
				headers
		);

		return restTemplate.exchange(
				BASE_REST_URL + "/payment/import",
				HttpMethod.POST,
				httpEntity,
				ValidationResult.class
		);
	}

	public LoanPaymentListItem getLoanPaymentListItem(Long loanPaymentId) {
		return restTemplate.getForObject(
				BASE_REST_URL + "/payment/" + loanPaymentId,
				LoanPaymentListItem.class
		);
	}

	public static ImportLoanPayment createImportLoanPayment(
			String importSourceCode,
			String importLoanCode,
			BigDecimal principal,
			BigDecimal interest,
			BigDecimal penalty
	) {
		BigDecimal total = principal.add(interest).add(penalty);
		return new ImportLoanPaymentBuilder()
				.importSourceCode(importSourceCode)
				.importLoanCode(importLoanCode)
				.paymentDate(LocalDate.now().minusDays(2))
				.principal(principal)
				.interest(interest)
				.penalty(penalty)
				.total(total)
				.build();
	}

	public Optional<LoanPayment> getPaymentByLoanId(Long loanId) {
		return loanPaymentRepository
				.findAll()
				.stream()
				.filter(item -> item.getLoan().getId().equals(loanId))
				.findFirst();
	}
}
