package acceptance.api;

import lv.wandoo.domain.dto.loan.ImportLoan;
import lv.wandoo.domain.dto.loan.ImportLoanBuilder;
import lv.wandoo.domain.dto.loan.InvestmentLoan;
import lv.wandoo.domain.model.Loan;
import lv.wandoo.domain.model.value.BorrowerDetails;
import lv.wandoo.domain.model.value.BorrowerDetailsBuilder;
import lv.wandoo.domain.model.value.PersonType;
import lv.wandoo.domain.model.value.TermUnit;
import lv.wandoo.domain.repository.LoanRepository;
import lv.wandoo.domain.validation.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static acceptance.common.BaseAcceptanceTest.BASE_REST_URL;

@Component
public class LoanTestAPI {
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private LoanRepository loanRepository;

	public ResponseEntity<ValidationResult> postLoanImportData(
			ImportLoan dto
	) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ImportLoan> httpEntity = new HttpEntity<>(
				dto,
				headers
		);

		return restTemplate.exchange(
				BASE_REST_URL + "/loan/import",
				HttpMethod.POST,
				httpEntity,
				ValidationResult.class
		);
	}

	public InvestmentLoan getInvestmentLoan(Long loanId) {
		return restTemplate.getForObject(
				BASE_REST_URL + "/loan/" + loanId,
				InvestmentLoan.class
		);
	}

	public List<InvestmentLoan> getInvestmentReadyList() {
		return Arrays.asList(restTemplate.getForObject(
				BASE_REST_URL + "/loan/investment-ready-list",
				InvestmentLoan[].class
		));
	}

	public Optional<Loan> getLoanByImportCredentials(
			String importSourceCode,
			String importLoanCode
	) {
		return loanRepository.getImportedLoan(importSourceCode, importLoanCode);
	}

	public Loan importTestLoan(
			BigDecimal amount,
			String importSourceCode, 
			String importLoanCode
	) {
		postLoanImportData(LoanTestAPI.createTestImportLoan(
				amount,
				importSourceCode,
				importLoanCode
		));

		return getLoanByImportCredentials(importSourceCode, importLoanCode)
				.orElse(null);
	}

	public static ImportLoan createTestImportLoan(
			BigDecimal amount,
			String importSourceCode,
			String importLoanCode
	) {
		BorrowerDetails borrowerDetails = new BorrowerDetailsBuilder()
				.personType(PersonType.PRIVATE)
				.companyName("sia test")
				.identificationNumber("4000000")
				.build();

		return new ImportLoanBuilder()
				.importSourceCode(importSourceCode)
				.importLoanCode(importLoanCode)
				.issueDate(LocalDate.now().minusDays(10))
				.countryCode("LV")
				.term(12)
				.termUnit(TermUnit.MONTH)
				.interestRate(12.8f)
				.amount(amount)
				.borrowerDetails(borrowerDetails)
				.build();
	}
}
