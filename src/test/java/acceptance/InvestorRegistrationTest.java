package acceptance;

import acceptance.api.InvestorTestAPI;
import acceptance.common.BaseAcceptanceTest;
import lv.wandoo.domain.dto.investor.InvestorProfile;
import lv.wandoo.domain.dto.investor.InvestorRegistration;
import lv.wandoo.domain.dto.investor.balance.InvestorBalance;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChange;
import lv.wandoo.domain.dto.investor.balance.InvestorFundsChangeBuilder;
import lv.wandoo.domain.model.Investor;
import lv.wandoo.domain.validation.ValidationResult;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;

@Transactional
public class InvestorRegistrationTest extends BaseAcceptanceTest {
	@Test
	public void registerAndAddFunds() {
		String investorEmail = "investor-registration-test@inbox.lv";
		InvestorRegistration investorRegistration 
				= InvestorTestAPI.createTestInvestorRegistration(investorEmail);

		ResponseEntity<ValidationResult> responseEntity
				= investorTestAPI.postRegistrationData(investorRegistration);

		assertTrue(responseOK(responseEntity));
		assertTrue(validationPassed(responseEntity));

		Optional<Investor> investor = investorTestAPI.getPersistedByEmail(investorEmail);
		assertTrue(investor.isPresent());
		Long investorId = investor.get().getId();
		InvestorProfile investorProfile = investorTestAPI.getInvestorProfile(investorId);

		assertNotNull(investorProfile);
		assertEquals(
				investorProfile.getEmail(),
				investorRegistration.getEmail()
		);

		InvestorBalance investorBalance = investorTestAPI.getInvestorBalance(investorId);
		assertNotNull(investorBalance);
		assertEquals(investorBalance.getAvailable(), BigDecimal.ZERO);

		BigDecimal addAmount = new BigDecimal("500");
		InvestorFundsChange investorFundsChange = new InvestorFundsChangeBuilder()
				.investorId(investorId)
				.amount(addAmount)
				.build();

		responseEntity = investorTestAPI.postFundsToInvestor(investorFundsChange);
		assertTrue(responseOK(responseEntity));
		assertTrue(validationPassed(responseEntity));

		investorBalance = investorTestAPI.getInvestorBalance(investorId);
		assertEquals(investorBalance.getAvailable(), addAmount);
	}
}
