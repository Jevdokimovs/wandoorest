package acceptance.common;

import acceptance.api.InvestorTestAPI;
import acceptance.api.LoanInvestmentTestAPI;
import acceptance.api.LoanPaymentTestAPI;
import acceptance.api.LoanTestAPI;
import lv.wandoo.domain.validation.ValidationResult;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = AcceptanceTestContext.class
)
@TestPropertySource(locations = "classpath:application-test.properties")
@Component
@Ignore
public class BaseAcceptanceTest {
	public final static String BASE_REST_URL = "/ws";

	@Autowired
	protected InvestorTestAPI investorTestAPI;

	@Autowired
	protected LoanTestAPI loanTestAPI;

	@Autowired
	protected LoanInvestmentTestAPI loanInvestmentTestAPI;
	
	@Autowired
	protected LoanPaymentTestAPI loanPaymentTestAPI;

	protected boolean responseOK(ResponseEntity responseEntity) {
		return responseEntity != null &&
				responseEntity.getStatusCode() == HttpStatus.OK;
	}

	protected boolean validationPassed(ResponseEntity responseEntity) {
		ValidationResult validationResult
				= (ValidationResult) responseEntity.getBody();

		return validationResult != null &&
				validationResult.getErrors().isEmpty();
	}
}
