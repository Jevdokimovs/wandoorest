package acceptance.common;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"lv.wandoo", "acceptance"})
@EnableAutoConfiguration
public class AcceptanceTestContext {
}
